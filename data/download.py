import requests
import os
from clint.textui import progress
import argparse

# add your datasets here
targets = [ 
    ('capitole.zip', 'https://cvg.ethz.ch/research/symmetries-in-sfm/datasets/capitole.zip'),
    ('cab.zip', 'https://cvg.ethz.ch/research/symmetries-in-sfm/datasets/CAB.zip'),
    ('synthetic.zip', 'https://prod-dcd-datasets-cache-zipfiles.s3.eu-west-1.amazonaws.com/fnxy8z8894-1.zip')
    # https://www.kaggle.com/competitions/image-matching-challenge-2022
]


# mostly taken from https://stackoverflow.com/questions/15644964/python-progress-bar-and-downloads
def delete_if_exists_and_download(filename, url):
    r = requests.get(url, stream=True)

    if (os.path.exists(filename)):
        os.remove(filename)

    print(f"downloading {filename}")

    with open(filename, 'wb') as f:
        total_length = int(r.headers.get('content-length'))
        for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length/1024) + 1): 
            if chunk:
                f.write(chunk)
                f.flush()


parser = argparse.ArgumentParser(
                    prog='Dataset Downloader',
                    description='downloads the datasets from internet',
                    epilog='')

parser.add_argument('--index', help='index of the function to run')
args = parser.parse_args()
index = args.index

if (index == None):
    [ delete_if_exists_and_download(filename, url) for filename, url in targets ]
else:
    index = int(index)
    delete_if_exists_and_download(targets[index][0], targets[index][1])
