FROM ubuntu:20.04 AS builder-image
ARG DEBIAN_FRONTEND=noninteractive
MAINTAINER Cedric Caspar

RUN apt-get update && apt-get install --no-install-recommends -y wget libegl1 libgl1 libgomp1 libglib2.0-0 libsm6 libxrender1 libxext6 python3.9 python3.9-dev python3.9-venv python3-pip python3-wheel build-essential cmake ninja-build git && \
	apt-get clean && rm -rf /var/lib/apt/lists/*

# create and activate virtual environment
# using final folder name to avoid path issues with packages
RUN python3.9 -m venv /home/myuser/venv
ENV PATH="/home/myuser/venv/bin:$PATH"

# install and build pybind11
#RUN cd /usr/local/src \
#    && wget https://github.com/pybind/pybind11/archive/v2.10.1.tar.gz \
#    && tar xvf v2.10.1.tar.gz \
#    && cd pybind11-2.10.1 \
#    && mkdir build \
#    && cd build \
#    && cmake .. \
#    && make \
#    && make install

# install requirements
RUN pip install --no-cache-dir wheel && python --version

RUN pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118

RUN pip install --upgrade pip

RUN pip install pytheia

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

#RUN useradd --create-home myuser
#RUN mkdir /home/myuser/code
#WORKDIR /home/myuser/code
#USER myuser

ENV PYTHONUNBUFFERED=1

CMD ["/bin/bash"]
#ENTRYPOINT ["/setup.sh"]
