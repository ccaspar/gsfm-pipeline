# NOTE: this example is not to be run. It is just a structural example to see how one should use
# our pipeline.

from src.pipeline import Pipeline
from src.intermediateStep import IntermediateStep
import src.helper

from src.stepImageOverlap.resNetGemPoolOverlap import ResNetGemPoolOverlap
from src.stepKeyPointsMatching.openCVKeyPointsMatching import OpenCVKeyPointsMatcher
from src.stepRelativePose.pyTheiaRelativePose import PyTheiaRelativePose
from src.stepRotationAveraging.pyTheiaRotationAveraging import PyTheiaRotationAveraging
from src.stepTranslationAveraging.pyTheiaTranslationAveraging import PyTheiaTranslationAveraging
from src.stepBundleAdjustment.pyTheiaBundleAdjustment import PyTheiaBundleAdjustment
from src.stepVisualize.meshVisualize import MeshVisualizer


# Bellow we create our own class that extends IntermediateStep object

class SomeRelativePoseStep(IntermediateStep):
    def __init__(self, someparams=None):
        super().__init__('SomeRelativePoseStepName', verbose=False)
    def run(self, pipeline_params, run_data):
        
        # can access the pipline params, see pipeline.py

        # can use data from run_data that was stored from previous steps
        # It is the step responsability to add the right data to the 
        # run_data dictionnary. Consult intermediateStep.py for more informations
        # basically, it contains two keys: camera vertices (ImageCamera class) and
        # camera edges (ImagePair class). The implemented step must know what data 
        # is available (set from the previous steps) and what data it has to set for
        # the next steps to work.

        # Important: must return run_data (will be forwarded to the next step)
        return run_data 

pipeline_params = {
    'image_loader': src.helper.DatasetLoader('some dataset path'),
    'verbose': True
}

# listing all the steps we want
steps = [
    ResNetGemPoolOverlap(),
    OpenCVKeyPointsMatcher(),
    SomeRelativePoseStep(),  # <--- Custom Step is inserted into Pipeline
    PyTheiaRotationAveraging(),
    PyTheiaTranslationAveraging(),
    PyTheiaBundleAdjustment(),
    MeshVisualizer()
]

# instanciating the pipeline
gsfm = Pipeline()
gsfm.run() # running the whole steps one by one
