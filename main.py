import pathlib
import sys

src_path = pathlib.Path('src').resolve()
if not src_path.exists():
    raise Exception('fail to find the src folder')

sys.path.append(str(src_path))

from src.util.default import DefaultSteps
from src.pipeline import Pipeline
import argparse

import src.helper




parser = argparse.ArgumentParser(description='run the pipeline with default steps')
parser.add_argument('--path', default='data/fountain', help='path to the dataset')
args = parser.parse_args()

dataset_path = pathlib.Path(args.path).resolve()
if (not dataset_path.exists()):
    raise Exception(f'failed to find the path {args.path}')

pipeline_params = {
    'image_loader': src.helper.DatasetLoader(dataset_path),
    'verbose': True
}

gsfm = Pipeline(pipeline_params, DefaultSteps.get())
gsfm.run()