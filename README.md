# gSFM-Pipeline

## Link to some Datasets
- https://www.cs.cornell.edu/projects/bigsfm/
- https://www.kaggle.com/competitions/image-matching-challenge-2022/data (used for the CSVOverlapMatcher example)
- http://cvg.ethz.ch/research/local-feature-evaluation/South-Building.zip
- http://www.ivl.disco.unimib.it/activities/evaluating-the-performance-of-structure-from-motion-pipelines/ (synthetic dataset)
- http://cvg.ethz.ch/research/local-feature-evaluation/South-Building.zip


## Create Environment
Create:
```bash
python -m venv <directory>
```
Activate:
```bash
source <directory>/bin/activate
```

## Installation
Python version: 3.9.1

Install dependencies
```
pip install -r requirements.txt
```

Initialize Submodules
```
git submodule init
git submodule update
```

## Usage

Run the main file in the root folder. Example as follows.

```python main.py --path=data/fountain```

The argument `--path` actually points to the dataset you want to run the pipeline on. Keep in mind that the folder must contains a `K.txt` file with a 3x3 K matrix loadable as numpy array (will be loaded automatically and if `K.txt` is not present then K is the identity matrix).

## Important note

The code that was written in this repo assumes that the user modified its python path to include the `src` folder. To run another file than main.py, please, modify your python path environment variable.

## Profiling some module
Using py-spy:
```
py-spy record -o profileoutput.svg -s  -- python <exec file>

```
Using cProfile:
```
python -m cProfile <exec file>

```
