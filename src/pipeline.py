import os.path
import time
import pickle

from intermediateStep import IntermediateStep
import helper



class Pipeline:
    """
    Class representing the global sfm pipeline.
    """
    def __init__(self, pipeline_params, steps, verbose=False):
        """
        TODO add doc
        """
        if steps is None:
            steps = []
        self.steps = steps
        self.checkpoint_path = "data/checkpoints/"
        self.use_checkpoint = set()
        self.pipeline_params = pipeline_params
        self.verbose = verbose

    def run(self):
        """
        TODO add doc
        """
        cur_input = {}
        start_time = time.time()
        step_time = time.time()
        for i, step in enumerate(self.steps):
            print(f"{'-' * 37}| Beginning Step {i}/{len(self.steps)-1}: {step.name} |{'-' * 37}")

            if self.hasCheckPoint(step):
                if self.verbose:
                    print(f"Loading step {type(step).__name__} from checkpoint")
                cur_input = self.loadCheckPoint(step)
            else:
                cur_input = step.run(self.pipeline_params, cur_input)
                if type(step).__name__ in self.use_checkpoint:
                    self.createCheckPoint(step, cur_input)

            print(f"{'-' * 37}| Finished Step {i}/{len(self.steps)-1} ({step.name}) after {time.time()-step_time} seconds |{'-' * 37}\n")
            step_time = time.time()
        print(f"{'-' * 128}")
        print(f"{'-' * 37}| Pipeline finished after {time.time()- start_time} seconds |{'-' * 37}")

        if self.verbose:
            print(f"{'-' * 37}| Results |{'-' * 37}")
            print(cur_input)

        return cur_input

    def addCheckpoint(self, name):
        self.use_checkpoint.add(name)

    def removeCheckpoint(self, name):
        if name in self.use_checkpoint:
            self.use_checkpoint.remove(name)

    def addCheckpointsForAll(self):
        for step in self.steps:
            self.use_checkpoint.add(type(step).__name__)

    def getStepPath(self, step):
        step_name = type(step).__name__ + ".cpickle"
        path = os.path.join(self.checkpoint_path, step_name)
        return path

    def hasCheckPoint(self, step):
        step_name = type(step).__name__
        path = self.getStepPath(step)
        if os.path.exists(path) and step_name in self.use_checkpoint:
            return True
        else:
            return False
    def createCheckPoint(self, step, data):
        path = self.getStepPath(step)
        with open(path, "wb+") as f:
            f.write(pickle.dumps(data))
        if self.verbose:
            print('Stored input to: ' + path)

    def loadCheckPoint(self, step):
        path = self.getStepPath(step)
        res = pickle.loads(open(path, "rb").read())
        # print('Loaded: ', res)
        return res

    @staticmethod
    def getDefaultParams(loader:helper.DatasetLoader):
        pipeline_params = {
            'image_loader': loader,
            'debug': False,
        }
        return pipeline_params
