from intermediateStep import IntermediateStep, InterStepDataMembers

import numpy as np
import helper as helper
import cv2
import gc
import pathlib

"""
References:

Flann & BF matcher Opencv python:
https://www.geeksforgeeks.org/python-opencv-drawmatchesknn-function/
https://docs.opencv.org/4.x/dc/dc3/tutorial_py_matcher.html
https://datahacker.rs/feature-matching-methods-comparison-in-opencv/

Lowe's paper:
https://link.springer.com/article/10.1023/B:VISI.0000029664.99615.94

OpenCV flann:
https://docs.opencv.org/3.4/d5/d6f/tutorial_feature_flann_matcher.html


potential doc for matchers: http://amroamroamro.github.io/mexopencv/opencv_contrib/SURF_descriptor.html
"""
#  see this file for outlier removal https://github.com/urbste/pyTheiaSfM/blob/master/pytest/sfm_pipeline.py

class Default:
    """
    utility class to make argument for the Opencv keypoint feature matching class
    """

    @staticmethod
    def BF() -> dict:
        sift = cv2.SIFT_create(nfeatures=0, nOctaveLayers=3, contrastThreshold=0.04, edgeThreshold=10, sigma=1.6)
        return {
            'ft_matcher': cv2.BFMatcher(cv2.NORM_L2),
            'ft_descriptor': sift,
            'lowe_thresh': 0.82,
            'debug': False
        }

    @staticmethod
    def FLANN() -> dict:
        sift = cv2.SIFT_create(nfeatures=0, nOctaveLayers=3, contrastThreshold=0.04, edgeThreshold=10, sigma=1.6)
        return {
            'ft_matcher': cv2.FlannBasedMatcher(),  # dict(algorithm=0, trees=20), dict(checks=150)
            'ft_descriptor': sift,
            'lowe_thresh': 0.82,
            'debug': False
        }


class OpenCVKeyPointsMatcher(IntermediateStep):
    """Represents step 1 of pipline using OpenCV"""

    def __init__(self, params=Default.BF(), verbose=False):
        super().__init__('OpenCVKeyPointsMatcher', verbose=verbose)

        self.params = params

        self.pipeline_tmp_path = helper.ROOT_DIR / 'tmp'  # helper.get_folder(helper.ROOT_DIR / 'tmp')
        self.debug_out_kp = self.pipeline_tmp_path / 'output_kp'
        self.debug_out_matchings = self.pipeline_tmp_path / 'output_matchings'

    def run(self, pipeline_params, run_data):
        """
        Finds the keypoints and descriptors for each image. Also finds the best matches
        according to the provided parameters.
        There are N images with M overlaps in totals. Moreover, each matchings is made out of a 
        list of best matches of some length T proper to each particlar matching.
        Arg:
            (M, 2) overlapping_images: list of tuples [(i, j)]
        Returns:
            dict : {
                (N, K, 2) keypoints: list of tuples containing the keypoints in pixel coordinates for each image
                    N: num images
                    K: num key points
                (M, T) matches : All accepted matches between each pair of overlapping images
                    M: num matchings (number of overlapping images)
                    T: maybe different for each overlap, the number of matched 2D correspondances.
            }
        """

        images = pipeline_params['image_loader'].images

        img_kp_descriptors = []
        for imageCamera in images:
            image = cv2.imread(imageCamera.path, cv2.IMREAD_GRAYSCALE)
            img_kp_descriptors.append(self.params['ft_descriptor'].detectAndCompute(image, None))

        if self.verbose:
            print("Computed keypoints and descriptors")

        if self.params['debug']:
            
            helper.mkdir_if_not_exists(self.pipeline_tmp_path)
            helper.mkdir_if_not_exists(self.debug_out_kp)

            for imageCamera in images:
                image = cv2.imread(imageCamera.path, cv2.IMREAD_GRAYSCALE)
                image_name = str(pathlib.Path(imageCamera.path).name)
                outimage = cv2.drawKeypoints(image, img_kp_descriptors[imageCamera.index][0], image, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
                cv2.imwrite(str((self.debug_out_kp / f"{image_name}").resolve()), outimage)

        gc.collect()

        # perform the actuall comparision between images
        good_cvmatches = [[] for _ in run_data[InterStepDataMembers.edges]]

        for index, imagePair in enumerate(run_data[InterStepDataMembers.edges]):
            _, desc1 = img_kp_descriptors[imagePair.image1.index]
            _, desc2 = img_kp_descriptors[imagePair.image2.index]
            # we match up to two candidates and will discard the worst
            matches = self.params['ft_matcher'].knnMatch(desc1, desc2, k=2)
            # from lowe's paper suggestion
            good_cvmatches[index] = [m for m, n in matches if (m.distance < self.params['lowe_thresh'] * n.distance)]

        if self.verbose:
            print("Computed matches")

        if self.params['debug']:
            if self.verbose:
                print("saving matchings to disk")

            helper.mkdir_if_not_exists(self.debug_out_matchings)

            # write each overlapping image with the retained matchings
            for match_index, image_pair in enumerate(run_data[InterStepDataMembers.edges]):
                
                img1 = cv2.imread(image_pair.image1.path)
                img2 = cv2.imread(image_pair.image2.path)

                index1 = image_pair.image1.index #image_names_to_ids[image_pair.image1.name]
                index2 = image_pair.image2.index #image_names_to_ids[image_pair.image2.name]

                img = cv2.drawMatches(img1, img_kp_descriptors[index1][0], img2, img_kp_descriptors[index2][0],
                                      good_cvmatches[match_index], outImg=img2.copy())
                
                cv2.imwrite(
                    str((self.debug_out_matchings / f"{image_pair.image1.img.split('.')[0]}vs{image_pair.image2.img.split('.')[0]}.png").resolve()),
                    img
                )

        gc.collect()

        # extracting from opencv only the keypoints of each image
        image_keypoints = [np.array([kp.pt for kp in kps], dtype=float) for kps in
                           [ img_kps for img_kps, _ in img_kp_descriptors ]]
        overlap_matches = [[(dmatch.queryIdx, dmatch.trainIdx) for dmatch in current_matching] for current_matching in
                           good_cvmatches]

        for i, imagePair in enumerate(run_data[InterStepDataMembers.edges]):
            # sets the keypoints to the image pair
            imagePair.image1.keyPoints = image_keypoints[imagePair.image1.index]
            imagePair.image2.keyPoints = image_keypoints[imagePair.image2.index]

            imagePair.matchings = overlap_matches[i]

            imagePair.outlier = True if len(overlap_matches[i]) < 8 else False
             
        return run_data


