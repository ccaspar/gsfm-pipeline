from intermediateStep import IntermediateStep, InterStepDataMembers, ImagePair

import numpy as np
import pathlib

class FountainKPMatchings(IntermediateStep):

    def __init__(self):
        super().__init__('FountainKeyPointsMatcher')


    def run(self, pipeline_params, run_data):
        
        loader = pipeline_params['image_loader']
        cameras = loader.images
        imagePairs = []

        # load keypoint for each image
        parent_path = pathlib.Path(loader.imgFolder)

        for cam in cameras:
            kp_path = (parent_path / (cam.img + ".txt")).resolve()
            cam.keyPoints = np.loadtxt(kp_path)

        cameras.sort(key=lambda x: x.img)
        for i, cam in enumerate(cameras):
            cam.index = i

        for i in range(len(cameras)):
            for j in range(i+1, len(cameras)):
                filename = f"000{i}.png-000{j}.png.txt"
                matchings_path = (parent_path / filename).resolve()
                cameraPair = ImagePair(cameras[i], cameras[j])
                matchingsnp = np.loadtxt(matchings_path)
                cameraPair.matchings = [(int(matchingsnp[idx][0]), int(matchingsnp[idx][1])) for idx in range(matchingsnp.shape[0])]
                cameraPair.outlier = False
                imagePairs.append(cameraPair)

        for image_pair in imagePairs:
            simple_correspondences = []
            for match in image_pair.matchings:
                point1 = np.array(image_pair.image1.keyPoints[match[0]])
                point2 = np.array(image_pair.image2.keyPoints[match[1]])
                simple_correspondences.append((point1, point2))
            image_pair.correspondences = simple_correspondences

        return {
            InterStepDataMembers.vertices: cameras,
            InterStepDataMembers.edges: imagePairs
        }