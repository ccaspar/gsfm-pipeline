from abc import ABC, abstractmethod
from PIL import Image

class IntermediateStepException(Exception):
    def __init__(self, msg=""):
        super().__init__(msg)

class PipelineException(Exception):
    def __init__(self, msg=""):
        super().__init__(msg)


class IntermediateStep(ABC):
    """
    Represents a specific step in the pipeline
    """

    def __init__(self, name='default Step', options:dict=None, verbose=False):
        """
        Args:
            name: The name of the step used for logging
            The step **might** have parameters intrisically related to it and that do not concern output of
            previous step nor the pipeline itself (ex. intrisic camera matrix in relative pose). In this case
            the constructor of the intermediate step must take the parameter upon construction.
        """
        self.name = name
        self.options = options
        self.verbose = verbose

    @abstractmethod
    def run(self, pipeline_params: dict, run_data: dict) -> dict:
        """
        Args:
        - pipeline_params : dict, general pipeline states/info/datastructure (image loading for example) that the step might use
        - run_params: dict, output of the last steps for this step execution
        """
        pass


"""
Class that holds the dictionary keys of each output/input steps.
This ensures that correct member key names are present and consistent along each pipeline steps
"""
class InterStepDataMembers:
    """
    Unified entry names of the dict for passing data between steps
    """

    def __init__(self):
        raise TypeError("Data class is not instantiable")

    vertices = 'imageCameraVertices'
    edges = 'imagePairs'
    adjacencyList = 'adjacencyList'
    reconstruction = 'points'


class ImageCamera:
    """
    Represents a camera in the pipeline with its associated image and its index
    Members:
    - index
        - index of the camera (must be unique)
    - imgName
        - str: name of the image with the extension(e.g. abcd.jpg)
    - path
        - str: absolute path to the image
    - K
        - (3, 3) numpy array representing the intrisics of the camera
    - globalRotation
        - (3, 3) numpy array. rotation of the camera in the world coordinates
    - globalTranslation
        - (3,) numpy array. Position of the camera in the world coordinates
    - keyPoints
        - numpy array of 2D positions
    - keyPointColors
        - RGB color of the keypoints 
    """
    def __init__(self, i:int, imgName: str, imgPath:str, cameraIntrinsics=None):
        """
        - i: int the index of the camera
        - imgName: str the name of the picture that is seen from the camera (e.g. abdc.jpg)
        - imgPath: str the full path (including the name) of the camera
        - cameraIntrinsics: (3, 3) numpy array. The K intrisics matrix (Quentin: why not name it K?)
        """

        self._initFlag = True # guard on

        self.index = i
        self.img = imgName
        self.path = imgPath
        img = Image.open(self.path)
        self.viewSize = img.size
        img.close()
        self.K = cameraIntrinsics

        self.width = 0
        self.height = 0

        self.globalFeatures = None # TODO add doc :)
        self.keyPoints = None
        self.keyPointColors = None

        self.globalRotation = None
        self.globalTranslation = None

        self._initFlag = False # guard off see __attr__

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.img == other.img
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return str(self.index)

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash(self.img)

    def clearFeatureCache(self):
        """
        Global features are only need in overlap step and never again. Remove to unnecessary data.
        """
        self.globalFeatures = None
        return self

    def __setattr__(self, attr, value):
        """
        The function `__setattr__` is called when self.some_member = some_value.
        This override avoid to define a new member to the class by accident.
        It enforces that new members may be added only inside __init__().
        Example:
        ```
            def __init(self):
                self._initFlag = True   
                self.member = None # valid
                self._initFlag = False
            ...
            # given instance "inst" of the object
            inst.member = 2 # valid (member exists already)
            inst.unexisting_member = None # raises exception
        ```
        The goal is to avoid having accidental definitions as
        `inst.SomeMember = ..` instead of `inst.someMember = ...`.
        The fundamental purpose is to force developers of the pipeline to instead rely 
        on using the return result (or argument of the step) of the step to increment
        thier own data results. 
        """
        if attr == '_initFlag' or hasattr(self, attr) or self._initFlag:
            object.__setattr__(self, attr, value)
        else:
            raise PipelineException(f'ImageCamera: the attribute {attr} was not found!')
        

class ImagePair:
    """
    Represents a camera view pairs. Contains the following members
    that are added during the pipeline:
    - self.matchings: list of tuples with corresponding keypoints (keypoint index image1, keypoint index image2)
    - self.outlier: bool
    - self.E: (3, 3) numpy array, essential matrix between image1 and image2
    - self.R: (3, 3) numpy array, relative rotation between image1 and image2
    - self.t: (3,) numpy array, relative translation between image1 and image2
    
    Note: image1 and image2 should be reference from the ImageCamera array and thus should
    not be overwritten in this class (under regular use of the pipeline).
    """
    def __init__(self, image1: ImageCamera, image2: ImageCamera, overlapScore=None):
        """
        - image1: ImageCamera the first camera view in the pair (ref from the main ImageCamera array)
        - image2: ImageCamera the second camera view in the pair (ref from the main ImageCamera array)
        - overlapping_score: int or None
        """

        self._initFlag = True # guard enabled, see __setattr__

        # define new members here

        self.image1 = image1
        self.image2 = image2
        self.overlapScore = overlapScore

        self.matchings = None
        self.correspondences = None

        self.outlier = False
        self.rotationOutlier = False
        self.translationOutlier = False

        self.E = None
        self.R = None
        self.t = None

        self._initFlag = False # guard disabled, see __setattr__

    def __str__(self):
        return str(self.image1.index) + ' <-> ' + str(self.image2.index)

    def __repr__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return (self.image1 == other.image1) and (self.image2 == other.image2) \
                   or (self.image1 == other.image2) and (self.image2 == other.image1)
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(str(self.image1) + str(self.image2))

    def __setattr__(self, attr, value):
        """
        The function `__setattr__` is called when self.some_member = some_value.
        This override avoid to define a new member to the class by accident.
        It enforces that new members may be added only inside __init__().
        Example:
        ```
            def __init(self):
                self._initFlag = True   
                self.member = None # valid
                self._initFlag = False
            ...
            # given instance "inst" of the object
            inst.member = 2 # valid (member exists already)
            inst.unexisting_member = None # raises exception
        ```
        The goal is to avoid having accidental definitions as
        `inst.SomeMember = ..` instead of `inst.someMember = ...`.
        The fundamental purpose is to force developers of the pipeline to instead rely 
        on using the return result (or argument of the step) of the step to increment
        thier own data results. 
        """
        if attr == '_initFlag' or hasattr(self, attr) or self._initFlag:
            object.__setattr__(self, attr, value)
        else:
            raise PipelineException(f'ImagePair: the attribute {attr} was not found!')
        

if __name__ == '__main__':

    print('run loading of the images')
    
    import helper as helper

    dataset = 'data/fountain'
    loader = helper.DatasetLoader(dataset)

    for image in loader.images:
        print(image)

    print('done loading toy dataset')