import os
import sys
import cv2
from imutils.paths import list_images
from pathlib import Path
from PIL import Image
import numpy as np
import pandas as pd
from intermediateStep import ImageCamera

def get_folder(folder):
    """
    returns the absolute path of the targetted folder
    Arg:
        folder: str
    Return:
        directory: pathlib.Path
    """

    if Path(folder).exists():
        return Path(folder).resolve()

    p = Path('.')
    candidates = list(p.glob(f'**/{folder}'))
    if len(candidates) == 0:
        raise Exception(f"failed to find folder {folder} from launching location")

    if len(candidates) != 1:
        raise Exception(f"Embiguity for {folder}, candidates: {candidates}")

    directory = candidates[0]

    if not directory.exists():
        raise Exception("failed to find the data folder from launching location")

    return directory


def mkdir_if_not_exists(directory):
    """
    mkdir given directory if the targetted Path does not exists
    Arg:
        directory : pathlib.Path
    return:
        Boolean: Whether the directory was created or not 
    """
    if not directory.exists():
        directory.mkdir()
        return True
    return False


DATA_DIR = get_folder('data')
ROOT_DIR = Path.cwd()  # get_folder('gsfm-pipeline')


class DatasetLoader:

    def __init__(self, dataset_folder):
        """
        Arg:
            dataset_folder: str target folder
            Quentin: TODO add doc and revisit naming
        """
        self.folderStr: str = dataset_folder
        self.folderPath: Path = get_folder(self.folderStr)
        self.imgFolder: Path = self.folderPath
        self.images: list[ImageCamera] = self._load()

    def listfiles(self):
        return [p.replace('\\', '/') for p in list_images(self.imgFolder)]

    def _load(self):
        """
        creates list of ImageCamera class. Will find the images in the set folderPath
        Note: if a calibration.csv is provided then intrisic matrices K (one for each view) will be set for each camera
        are expected to be avaialble. If a file K.txt is available then a single K matrix will be set (the same) for all
        cameras. If not such files are provided then K=I.
        Return:
            list of initialized ImageCamera
        """
        imgs = self.listfiles()
        if len(imgs) <= 1:
            try:
                self.imgFolder = get_folder(os.path.join(self.folderStr, 'images').replace('\\','/'))
            except Exception:
                raise Exception('No images found')
            imgs = self.listfiles()

        intrinsics_path = str((self.folderPath / "calibration.csv").resolve())
        calibration = None
        if os.path.isfile(intrinsics_path): # if a calibration.csv file is provided
            # read Calibration Data from CSV
            calibration = pd.read_csv(intrinsics_path)
            # Set File Name of Image as Index
            calibration = calibration.set_index('image_id')
            # Drop everything but the intrinsic matrices
            calibration = calibration['camera_intrinsics']
            # Translate to dict for easy lookup
            intrinsics = calibration.to_dict()
            # Intrinsic Matrices are still strings so we have to convert to 3by3 numpy arrays
            for image_id, intrinsic_matrix in intrinsics.items():
                intrinsics[image_id] = np.array(intrinsic_matrix.split(),dtype=float).reshape((3,3))
            
            # Quentin: TODO maybe use pathlib here the code is hard to read
            result = [ImageCamera(i, p[p.replace("\\", "/").rfind("/") + 1:], p, intrinsics[p[p.replace("\\", "/").rfind("/") + 1:].split('.')[0]]) for i, p in enumerate(imgs)]
        else:

            # The same K matrix is being used
            # either a K.txt was specified or the identity matrix is used.

            K_path = (self.folderPath / "K.txt").resolve()
            K = np.identity(3)
            if K_path.exists() and K_path.is_file():
                K = np.loadtxt(K_path)
            result = [ImageCamera(i, p[p.replace("\\", "/").rfind("/") + 1:], p, cameraIntrinsics=K) for i, p in enumerate(imgs)] # no per camera intrinsic

        # set the dimensions of the images
        for image in result:
            opened = Image.open(image.path)
            image.width, image.height = opened.size

        return result
