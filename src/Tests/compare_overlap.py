import csv
import sys
im_list = {}

if len(sys.argv) != 2:
    test_file = "../../test.txt"
else:
    test_file = sys.argv[1]

# our overlap solution written to a file
with open(test_file) as file:
    data = file.read().rstrip()
    im_list = eval(data)
    count = 0
    for i in im_list.keys():
        count += len(im_list[i])
    print(f"Ovlerapping immages from our approach: {count}")

# create list of all pairs from brandenburg_gate
csv_list = {}
csv_path = "data/train/brandenburg_gate/pair_covisibility.csv"
with open(csv_path, 'r') as f:
    csvreader = csv.DictReader(f)
    for row in csvreader:
        pair = row['pair'].split('-')
        pair0 = f"{pair[0]}.jpg"
        pair1 = f"{pair[1]}.jpg"

        # check if node already exists in dict
        if pair0 not in csv_list:
            csv_list[pair0] = [pair1]
        else:
            csv_list[pair0].append(pair1)

print("Comparing lists...")
error_count = 0
pos_count = 0
key_error = 0
for key in im_list.keys():
    if key not in csv_list:
        key_error += 1
    else:
        for item in im_list[key]:
            if item in csv_list[key]:
                pos_count += 1
            else:
                if key in csv_list[item]:
                    pos_count += 1
                else:
                    error_count += 1

print(f"Matches overlapping with ground truth: {pos_count}")
print(f"Images not corresponding to ground truth: {error_count}")
print(f"Images not found as key in ground truth: {key_error}")
