
from intermediateStep import IntermediateStep, InterStepDataMembers, ImagePair, ImageCamera
import pathlib
from helper import get_folder
from SynteticHelper import *
import pytheia as pt

dataset = 'data/fountain'

try:
    fountain_ds_path = pathlib.Path(get_folder(dataset)).resolve()
except:
    print("make sure that the fountain dataset is present")


class LoadSyntetic(IntermediateStep):

    def __init__(self):
        super().__init__('FountainP11Syntetic')

    def run(self, pipeline_params: dict, run_data: dict):

        K = loadKMatrix(fountain_ds_path)
        t_gt, R_gt, P_gt = loadProjectionsMatrices(K, fountain_ds_path)

        edges: list[ImagePair] = run_data[InterStepDataMembers.edges]
        vertices: list[ImageCamera] = run_data[InterStepDataMembers.vertices]

        for i, vertex in enumerate(vertices):
            vertex.globalRotation = R_gt[i]
            vertex.globalTranslation = t_gt[i]

        #for i, edge in enumerate(edges):
        #    edge


        return run_data
    

class RemoveRecon(IntermediateStep):

    def __init__(self):
        super().__init__('RemoveRecon')

    def run(self, pipeline_params: dict, run_data: dict):
        if 'recon' in run_data:
            del run_data['recon']
        return run_data

showPoints = False

def getDefaultOptions(verbose=True):
    options = pt.sfm.BundleAdjustmentOptions()
    #options.function_tolerance = math.inf
    #options.gradient_tolerance = math.inf
    options.loss_function_type = pt.sfm.LossFunctionType.HUBER
    #options.intrinsics_to_optimize = pt.sfm.OptimizeIntrinsicsType.ALL
    options.verbose = verbose
    options.robust_loss_width = 3.0
    #options.constant_camera_orientation = True
    #options.constant_camera_position = True
    options.use_position_priors = True 
    options.max_num_iterations = 10

    return {
        'pythiaOptions' : options,
        'min_observations' : 6
    }

if __name__ == '__main__':
    
    print('running bundle adjust syntetic')
    from helper import DatasetLoader
    from stepBundleAdjustment.pyTheiaBundleAdjustment import PyTheiaBundleAdjustment
    from stepVisualize.pointCloudVisualize import PointCloudVisualizer
    from stepKeyPointsMatching.fountainKPMatching import FountainKPMatchings
    from pipeline import Pipeline
    from stepCreateRecon.pyTheiaCreateRecon import TheiaCreateRecon
    from util.storageStep import StoreIntermediateData, LoadIntermediateData

    pipeline_params = {
        'image_loader': DatasetLoader(dataset),
        'debug' : True,
        'verbose' : True
    }

    if (not showPoints):

        from stepKeyPointsMatching.fountainKPMatching import FountainKPMatchings
        from stepKeyPointsMatching.openCVKeyPointsMatching import OpenCVKeyPointsMatcher
        from stepRelativePose.openCVRelativePose import OpenCVRelativePoseEstimator
        from stepRotationAveraging.pyTheiaRotationAveraging import TheiaRotationAveraging
        from stepTranslationAveraging.pyTheiaTranslationAveraging import TheiaTranslationAveraging
        from stepRelativePose.pyTheiaRelativePose import PyTheiaRelativePose
        from stepImageOverlap.resNetGemPoolOverlap import ResNetGemPoolOverlap

        steps = [
            #ResNetGemPoolOverlap(),
            #FountainKPMatchings(),
            #OpenCVKeyPointsMatcher(), #FountainKPMatchings(),
            FountainKPMatchings(),
            PyTheiaRelativePose(verbose=True),
            TheiaRotationAveraging(),
            TheiaTranslationAveraging(),
            LoadSyntetic(),
            TheiaCreateRecon(),
            PyTheiaBundleAdjustment(options=getDefaultOptions(), verbose=True, triangulate_only=True),
            RemoveRecon(),
            StoreIntermediateData('data/checkpoint', 'synteticFountain'),
            PointCloudVisualizer()
        ]

        """
        steps = [
            FountainKPMatchings(),
            LoadSyntetic(),
            TheiaCreateRecon(),
            PyTheiaBundleAdjustment(options=getDefaultOptions(), verbose=True, triangulate_only=True),
            RemoveRecon(),
            StoreIntermediateData('data/checkpoint', 'synteticFountain'),
            PointCloudVisualizer()
        ]
        """
        """
        steps = [
            FountainKPMatchings(),
            LoadSyntetic(),
            TheiaCreateRecon(),
            PyTheiaBundleAdjustment(options=getDefaultOptions(), verbose=True, triangulate_only=True),
            RemoveRecon(),
            StoreIntermediateData('data/checkpoint', 'synteticFountain'),
            PointCloudVisualizer()
        ]
        """
    else:
        steps = [
            LoadIntermediateData('data/checkpoint', 'synteticFountain'),
            PointCloudVisualizer()
        ]

    gsfm = Pipeline(pipeline_params, steps)
    #gsfm.addCheckpointsForAll()
    pipeline_result = gsfm.run()
    

