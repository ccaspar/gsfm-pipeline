import numpy as np
import pandas as pd

import os
from intermediateStep import IntermediateStep

'''
Comparing Matrices
https://math.stackexchange.com/questions/2113634/comparing-two-rotation-matrices


'''


class RotationTest(IntermediateStep):
    """Tester to score accuracy of relative pose estimation"""

    def __init__(self, params: dict):
        super().__init__('RotationTest')
        self.params = params

    def rotationTestMatrix(self, image_pairs, groundtruth_path):
        if not os.path.isfile(groundtruth_path):
            print("No Groundtruth Data found...")
            return
        
        errors = []
        pairs = []

        # read groundtruth Data from CSV
        gt = pd.read_csv(groundtruth_path)
        # Set File Name of Image as Index
        gt = gt.set_index('image_id')
        # Drop everything but the rotation matrices
        gt = gt['rotation_matrix']
        # Translate to dict for easy lookup
        gt_rotations = gt.to_dict()
        # Rotation Matrices are still strings so we have to convert to 3by3 numpy arrays
        for image_id, rotation_matrix in gt_rotations.items():
            gt_rotations[image_id] = np.array(rotation_matrix.split(),dtype=float).reshape((3,3))

        for pair in image_pairs:

            # get index from im1
            img1_idx = pair.image1.img.split('.')[0]
            # get index from im2
            img2_idx = pair.image2.img.split('.')[0]

            r1 = gt_rotations[img1_idx]
            r2 = gt_rotations[img2_idx]

            # get relative rotation of given indices
            R_groundTruth = np.conjugate(r1) * r2

            if self.params['verbose']:
                print('img1 Index: ', img1_idx)
                # print('img1 Ground Truth: ', data1)
                print('img2 Index: ', img2_idx)
                # print('img2 Ground Truth: ', data2)
                print('True Rotation:\n', R_groundTruth)
                print('Estimated Rotation:\n', pair.R)
                print('\n')

            # Calculate Rotation Error (See stack overflow for explanation)
            R_ab = np.matmul(np.transpose(R_groundTruth), pair.R)

            pairs.append(str(img1_idx) + " vs " + str(img2_idx))
            errors.append(np.rad2deg(np.arccos((np.trace(R_ab) - 1) / 2)))

        return pd.DataFrame({'Error': errors, 'Pairing': pairs})

    def rotationTestQuaternion(self, image_pairs, true_rotations):
        errors = []
        pairs = []

        for pair in image_pairs:

            # get index from im1
            img1_idx = int(pair.image1['name'][0:4])

            # get quaternion rotation form im1
            data1 = true_rotations.loc[true_rotations['image_number'] == img1_idx]
            q1 = np.quaternion(data1['rotation_w'].iloc[0], data1['rotation_x'].iloc[0], data1['rotation_y'].iloc[0],
                               data1['rotation_z'].iloc[0])

            # get index from im2
            img2_idx = int(pair.image2['name'][0:4])

            # get quaternion rotation from im2
            data2 = true_rotations.loc[true_rotations['image_number'] == img2_idx]

            q2 = np.quaternion(data2['rotation_w'].iloc[0], data2['rotation_x'].iloc[0], data2['rotation_y'].iloc[0],
                               data2['rotation_z'].iloc[0])

            # get relative quaternion rotation of given indices and convert to matrix
            R_groundTruth = quaternion.as_rotation_matrix(np.conjugate(q1) * q2)

            if self.params['debug']:
                print('img1 Index: ', img1_idx)
                # print('img1 Ground Truth: ', data1)
                print('img2 Index: ', img2_idx)
                # print('img2 Ground Truth: ', data2)
                print('True Rotation:\n', R_groundTruth)
                print('Estimated Rotation:\n', pair.R)
                print('\n')

            # Calculate Rotation Error (See stack overflow for explanation)
            R_ab = np.matmul(np.transpose(R_groundTruth), pair.R)

            pairs.append(str(img1_idx) + " vs " + str(img2_idx))
            errors.append(np.rad2deg(np.arccos((np.trace(R_ab) - 1) / 2)))

        return pd.DataFrame({'Error': errors, 'Pairing': pairs})
    


    def run(self, pipeline_params: dict, run_data: dict):
        import quaternion
        rot_errors = None

        groundtruth_path = str((pipeline_params['image_loader'].folderPath / "calibration.csv").resolve())

        if self.params['method'] == 'quaternion':
            rot_errors = self.rotationTestQuaternion(run_data['overlappingImagePairs'], self.params['syntetic_data'])

        if self.params['method'] == 'matrix':
            rot_errors = self.rotationTestMatrix(run_data['overlappingImagePairs'], groundtruth_path)

        if self.params['debug']:
            print(rot_errors.to_string(), '\n')
            print("rot_error Statistics:\n", rot_errors.describe())
        return run_data
