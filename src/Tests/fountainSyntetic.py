from intermediateStep import IntermediateStep, InterStepDataMembers, ImageCamera
import pathlib
from helper import get_folder
from pprint import pprint
import numpy as np
from scipy.spatial.transform import Rotation as R

dataset = 'data/fountain'

try:
    fountain_ds_path = pathlib.Path(get_folder(dataset)).resolve()
except:
    print("make sure that the fountain dataset is present")




if __name__ == '__main__':

    # run the pipeline 
    from pipeline import Pipeline
    from stepImageOverlap.resNetCosineOverlap import ResNetCosineOverlap
    from stepKeyPointsMatching.fountainKPMatching import FountainKPMatchings
    from stepKeyPointsMatching.openCVKeyPointsMatching import OpenCVKeyPointsMatcher
    from stepRelativePose.openCVRelativePose import OpenCVRelativePoseEstimator
    from stepRotationAveraging.pyTheiaRotationAveraging import TheiaRotationAveraging
    from stepTranslationAveraging.pyTheiaTranslationAveraging import TheiaTranslationAveraging
    from util.printStep import PrintIntermediateData
    from util.storageStep import StoreIntermediateData
    from stepRelativePose.pyTheiaRelativePose import PyTheiaRelativePose
    from helper import DatasetLoader
    import matplotlib.pyplot as plt
    from SynteticHelper import *

    pipeline_params = {
        'image_loader': DatasetLoader(dataset),
        'debug' : True,
        'verbose' : True
    }

    steps = [
        #ResNetCosineOverlap(),
        #OpenCVKeyPointsMatcher(),
        FountainKPMatchings(),
        PyTheiaRelativePose(), #OpenCVRelativePoseEstimator(),
        TheiaRotationAveraging(),
        TheiaTranslationAveraging()
    ]

    gsfm = Pipeline(pipeline_params, steps)
    pipeline_result = gsfm.run()

    K = loadKMatrix(fountain_ds_path)
    t_gt, R_gt, P_gt = loadProjectionsMatrices(K, fountain_ds_path)

    t_estimates, R_estimates, P_estimates = extractPipelineResults(K, pipeline_result)

    t_estimates = -t_estimates
    tmp = t_estimates[:, 1].copy()
    t_estimates[:, 1] = t_estimates[:, 2]
    t_estimates[:, 2] = tmp

    trans_sd = np.linalg.norm(t_estimates - t_gt, axis=1)**2
    rotations_sd = np.linalg.norm(R_gt - R_estimates, axis=(1, 2))**2
    projection_sd = np.linalg.norm(P_gt - P_estimates, axis=(1, 2))**2

    trans_mse = np.mean(trans_sd)
    rotation_mse = np.mean(rotations_sd)
    projection_mse = np.mean(projection_sd)

    print(f'mean square error: translation {np.mean(trans_sd)}, rotation {np.mean(rotations_sd)}, projection {np.mean(projection_sd)}')
    print(f'median square error: translation {np.median(trans_sd)}, rotation {np.median(rotations_sd)}, projection {np.median(projection_sd)}')
    
    fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(7, 9))
    index = np.arange(10)

    # ground truth

    plot_translations('translation.png', t_gt, t_estimates)
    plot_rotations('view', R_gt, R_estimates)