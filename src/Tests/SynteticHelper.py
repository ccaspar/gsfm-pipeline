from intermediateStep import IntermediateStep, InterStepDataMembers, ImageCamera
import pathlib
from helper import get_folder
from pprint import pprint
import numpy as np
from scipy.spatial.transform import Rotation as R
import matplotlib.pyplot as plt


def loadProjectionsMatrices(K, ds_path):
    """
    Loads the projection matrices of the fountain P11 dataset
    """
    P_matrix_paths = sorted(list(ds_path.glob('./*.P')))
    P_matrix_paths = P_matrix_paths[:-1]
    pprint(P_matrix_paths)
    P = [] #np.empty((10, 4, 4))
    t = []
    R = []

    assert np.linalg.det(K) > 0.0, "K matrix must be invertible"
    K_inv = np.linalg.inv(K)

    for path in P_matrix_paths:
        #P = np.concatenate((P, np.loadtxt(path)[:, :, None]), axis=0)
        projection = np.loadtxt(path)
        P.append(projection)
        t.append(projection[:3, 3])
        Rotation = K_inv @ projection[:3,:3]

        assert np.isclose(np.linalg.det(Rotation), 1.0), "R must be a rotation matrix"
        R.append(Rotation)
    
    P = np.array(P)
    R = np.array(R)
    t = np.array(t)

    return t, R, P

def loadKMatrix(ds_path):
    """
    Loads the K matrix
    """
    return np.loadtxt(ds_path / 'K.txt')

def extractPipelineResults(K, pipeline_result):
    
    if not InterStepDataMembers.vertices in pipeline_result or not InterStepDataMembers.edges in pipeline_result:
        raise Exception(f'missing {InterStepDataMembers.vertices} or {InterStepDataMembers.edges}')

    cameras: list[ImageCamera] = pipeline_result[InterStepDataMembers.vertices]

    t_estimates = [] #np.empty((10, 3)) #[]
    R_estimates = [] # np.empty((10, 3, 3)) # []
    P_estimates = [] #np.empty((10, 4, 4)) #[]

    for cam in cameras:

        #t_estimates = np.concatenate((t_estimates, cam.globalTranslation), axis=0)
        #R_estimates = np.concatenate((R_estimates, cam.globalRotation), axis=0)

        t_estimates.append(cam.globalTranslation)  
        R_estimates.append(cam.globalRotation)

        KR = K @ cam.globalRotation
        proj = np.zeros((3, 4))
        proj[0,:3] = KR[0]
        proj[1,:3] = KR[1]
        proj[2,:3] = KR[2]
        proj[:3, 3] = cam.globalTranslation
        
        P_estimates.append(proj) #

    return np.array(t_estimates), np.array(R_estimates), np.array(P_estimates)
    



def save_figure(name):
    temp_folder = get_folder('tmp')
    if (not temp_folder.exists()):
        temp_folder.mkdir()
    temp_folder = temp_folder.resolve()
    plt.tight_layout()
    plt.savefig(temp_folder / f'{name}', bbox_inches='tight', dpi=300)

def plot_translations(name, t_gt, t_esimated):
    
    plt.clf()
    fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(7, 9))
    index = np.arange(t_gt.shape[0])

    # ground truth

    axes[0, 0].plot(index, t_gt[:, 0])
    axes[0, 0].set_xlabel('camera index')
    axes[0, 0].set_ylabel('x position')
    axes[0, 0].set_title('ground truth translation (x)')
    axes[0, 0].grid(True)

    axes[1, 0].plot(index, t_gt[:, 1])
    axes[1, 0].set_xlabel('camera index')
    axes[1, 0].set_ylabel('y position')
    axes[1, 0].set_title('ground truth translation (y)')
    axes[1, 0].grid(True)

    axes[2, 0].plot(index, t_gt[:, 2])
    axes[2, 0].set_xlabel('camera index')
    axes[2, 0].set_ylabel('z position')
    axes[2, 0].set_title('ground truth translation (z)')
    axes[2, 0].grid(True)

    # estimate
    
    axes[0, 1].plot(index, t_esimated[:, 0])
    axes[0, 1].set_xlabel('camera index')
    axes[0, 1].set_ylabel('x position')
    axes[0, 1].set_title('estimate translation (x)')
    axes[0, 1].grid(True)

    axes[1, 1].plot(index, t_esimated[:, 1])
    axes[1, 1].set_xlabel('camera index')
    axes[1, 1].set_ylabel('y position')
    axes[1, 1].set_title('estimate translation (y)')
    axes[1, 1].grid(True)

    axes[2, 1].plot(index, t_esimated[:, 2])
    axes[2, 1].set_xlabel('camera index')
    axes[2, 1].set_ylabel('z position')
    axes[2, 1].set_title('estimate translation (z)')
    axes[2, 1].grid(True)
    save_figure(name)

def PlotCamera(R, t, ax, scale=1.0, color='b'):
    
    assert ax is not None
    
    #if ax == None:
    #    fig = plt.figure()
    #    ax = fig.add_subplot(111, projection='3d')

    Rcw = R.transpose()
    tcw = -Rcw @ t

    # Define a path along the camera gridlines
    camera_points = np.array([
        [0, 0, 0],
        [1, 1, 1],
        [1, -1, 1],
        [-1, -1, 1],
        [-1, 1, 1],
        [1, 1, 1],
        [0, 0, 0],
        [1, -1, 1],
        [0, 0, 0],
        [-1, -1, 1],
        [0, 0, 0],
        [-1, 1, 1]
    ])

    # Make sure that this vector has the right shape
    tcw = np.reshape(tcw, (3, 1))

    cam_points_world = (Rcw @ (scale * camera_points.transpose()) + np.tile(tcw, (1, 12))).transpose()

    ax.plot(xs=cam_points_world[:,0], ys=cam_points_world[:,1], zs=cam_points_world[:,2], color=color)

    #plt.show(block=False)

    return ax

def convert_to_rotvec(rotations):
    assert rotations.shape[1] == 3 and rotations.shape[2] == 3
    rotvecs = []
    for i in range(rotations.shape[0]):
        rotvecs.append(R.as_rotvec(rotations[i]))
    return np.array(rotvecs)

def plot_rotations(name, R_gt, R_esimated):
    plt.clf()
    #fig, axes = plt.subplots(nrows=R_gt.shape[0], ncols=1, figsize=(7, 9))
    #index = np.arange(t_gt.shape[0])

    for i in range(R_gt.shape[0]):
        print(f'{i}: {np.linalg.det(R_gt[i])}, axis: {R.from_matrix(R_gt[i]).as_rotvec()}')

    for i in range(R_esimated.shape[0]):
        print(f'{i}: {np.linalg.det(R_esimated[i])}, axis: {R.from_matrix(R_esimated[i]).as_rotvec()}')

    for i in range(R_esimated.shape[0]):
        
        fig = plt.figure(figsize=(12, 12))
        plt.tight_layout()
        ax = fig.add_subplot(R_esimated.shape[0], 1, i+1, projection='3d')
        
        ax.set_title(f'view {i}')
        ax.set_xlabel('$x$')
        ax.set_ylabel('$y$')
        ax.set_zlabel('$z$')
        
        #print(f'axis x: {np.dot(R_esimated[i, :, 0], R_gt[i, :, 0])}')
        #print(f'axis y: {np.dot(R_esimated[i, :, 1], R_gt[i, :, 0])}')
        #print(f'axis z: {np.dot(R_esimated[i, :, 2], R_gt[i, :, 0])}')

        #ax.set_xlim([0, 1])
        #ax.set_ylim([0, 1])
        PlotCamera(R_esimated[i], np.array([0, 0, 0]), ax, scale=1, color='b')
        PlotCamera(R_gt[i], np.array([0, 0, 0]), ax, scale=1, color='r')
        save_figure(f'view_{i}.png')
