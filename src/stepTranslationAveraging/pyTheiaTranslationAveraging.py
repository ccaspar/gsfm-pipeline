import pytheia as pt
from scipy.spatial.transform import Rotation

from intermediateStep import IntermediateStep, InterStepDataMembers

class PyTheiaTranslationAveraging(IntermediateStep):

    def __init__(self, verbose=False):
        super().__init__('pyTheiaTranslationAveraging', verbose=verbose)

    def run(self, pipeline_params: dict, run_data: dict):
        # init the options to the position estimator
        position_estimator_options = pt.sfm.LeastUnsquaredDeviationPositionEstimatorOptions()
        position_estimator_options.max_num_iterations = 400
        position_estimator_options.max_num_reweighted_iterations = 10
        position_estimator_options.convergence_criterion = 1e-4
        position_estimator = pt.sfm.LeastUnsquaredDeviationPositionEstimator(position_estimator_options)

        view_pairs = {}
        rotations = {}

        for imagePair in run_data[InterStepDataMembers.edges]:
            id1 = imagePair.image1.index
            id2 = imagePair.image2.index

            two_view_info = pt.sfm.TwoViewInfo()

            two_view_info.focal_length_1 = imagePair.image1.K[0][0]
            two_view_info.focal_length_2 = imagePair.image2.K[0][0]
            two_view_info.position_2 = imagePair.t
            two_view_info.rotation_2 = imagePair.R

            view_pairs[(id1, id2)] = two_view_info
            rotations[id1] = Rotation.from_matrix(imagePair.image1.globalRotation).as_rotvec()
            rotations[id2] = Rotation.from_matrix(imagePair.image2.globalRotation).as_rotvec()

        result = position_estimator.EstimatePositions(view_pairs, rotations)

        for key, value in result.items():
            run_data[InterStepDataMembers.vertices][key].globalTranslation = value

        return run_data


def makeRelativePoseCheckPoint(folder_name):

    from src import helper
    from pipeline import Pipeline
    import numpy as np
    
    from util.printStep import PrintIntermediateData
    from util.storageStep import StoreIntermediateData

    from util.default import DefaultSteps

    pipeline_params = {
        'image_loader': helper.DatasetLoader(folder_name)
    }

    steps = DefaultSteps.get(until=4)
    # adding print steps in between
    steps = [ [ step, PrintIntermediateData() ] for step in steps ]
    steps = [item for sublist in steps for item in sublist]
    steps += [ StoreIntermediateData('data/checkpoints/translation_averaging', 'rotationAveragingData') ]

    gsfm = Pipeline(pipeline_params, steps)
    res = gsfm.run()

    return res
