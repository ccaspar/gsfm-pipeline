from intermediateStep import IntermediateStep, InterStepDataMembers
import numpy as np
import open3d as o3d

class PointCloudVisualizer(IntermediateStep):
    def __init__(self):
        super().__init__('PyVistaVisualizer')

    def getBasisLineSet(self, cameras, factor=0.1, axis=0):

        factor = 1.0
        camera_basis = np.array([cameras[i].globalTranslation + cameras[i].globalRotation[axis, :] * factor for i, _ in enumerate(cameras)])
        camera_positions = np.array([cameras[i].globalTranslation for i, _ in enumerate(cameras)])
        
        color = [0, 0, 0]
        color[axis] = 1
        
        colors = [ color for i in range(camera_basis.shape[0])]
        indices = [[i, i + camera_basis.shape[0]] for i in range(camera_basis.shape[0])]
        camera_basis = np.concatenate((camera_basis, camera_positions), axis=0)
        
        line_set = o3d.geometry.LineSet()
        line_set.points = o3d.utility.Vector3dVector(camera_basis)
        line_set.lines = o3d.utility.Vector2iVector(indices)
        line_set.colors = o3d.utility.Vector3dVector(colors)

        return line_set

    def run(self, pipeline_params: dict, run_data: dict):
        # if experiencing problems with pyvista where MESA driver
        # cannot load see https://stackoverflow.com/questions/71010343/cannot-load-swrast-and-iris-drivers-in-fedora-35/72200748#72200748

        pcd = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(run_data['points'])
        pcd.colors = o3d.utility.Vector3dVector(np.ones_like(run_data['points']) * 0.5)

        camera_positions = np.array([cam.globalTranslation for cam in run_data[InterStepDataMembers.vertices]])
        camColors = np.zeros_like(camera_positions)
        camColors[:, 0] = 1.0

        pcdCam = o3d.geometry.PointCloud()
        pcdCam.points = o3d.utility.Vector3dVector(camera_positions)
        pcdCam.colors = o3d.utility.Vector3dVector(camColors)

        factor = 0.1
        lx = self.getBasisLineSet(run_data[InterStepDataMembers.vertices], factor=factor, axis=0)
        ly = self.getBasisLineSet(run_data[InterStepDataMembers.vertices], factor=factor, axis=1)
        lz = self.getBasisLineSet(run_data[InterStepDataMembers.vertices], factor=factor, axis=2)

        # show the result
        o3d.visualization.draw_geometries([pcd, pcdCam, lx, ly, lz], zoom=10.0, front=[-0.12457451465734075, -0.95639571618052521, -0.26417498816017471],
                                          lookat=[3.5326351281197632, 1.5228274387026011, -2.511915145878032],
                                          up=[0.28835707715770814, -0.28965438634108875, 0.91266123645437636])


if __name__ == '__main__':

    print('running bundle ajuster from pytheia')
    
    import pathlib
    # from util.default import DefaultSteps
    import helper as helper
    # from util.storageStep import LoadIntermediateData
    from pipeline import Pipeline
    from stepBundleAdjustment.pyTheiaBundleAdjustment import PyTheiaBundleAdjustment
    """
    dataset = 'data/fountain'
    checkpoint_folder_path = pathlib.Path('data/checkpoints/bundleAdjustment/pytheiaBA.cpickle').resolve()
    checkpoint_folder = str(checkpoint_folder_path.parent)
    checkpoint_folder_name = checkpoint_folder_path.name.removesuffix('.cpickle')
    DefaultSteps.makeCheckPoint(dataset, checkpoint_folder, checkpoint_folder_name, until=5)

    pipeline_params = {
        'image_loader': helper.DatasetLoader(dataset)
    }

    steps = [
        LoadIntermediateData(checkpoint_folder, checkpoint_folder_name),
        PyTheiaBundleAdjustment(),
        PyVistaVisualizer()
    ]

    gsfm = Pipeline(pipeline_params, steps)
    res = gsfm.run()

    print(res)
    """
    """
    pipeline_params = {
        'image_loader': helper.DatasetLoader(dataset)
    }
    """
    """
    steps = [
        LoadIntermediateData(checkpoint_folder, checkpoint_folder_name),
        PyTheiaBundleAdjustment()
    ]
    """

    dataset = 'data/fountain'

    from stepKeyPointsMatching.fountainKPMatching import FountainKPMatchings
    from stepRelativePose.openCVRelativePose import OpenCVRelativePoseEstimator
    from stepRotationAveraging.pyTheiaRotationAveraging import TheiaRotationAveraging
    from stepTranslationAveraging.pyTheiaTranslationAveraging import TheiaTranslationAveraging
    from util.printStep import PrintIntermediateData
    from util.storageStep import StoreIntermediateData

    pipeline_params = {
        'image_loader': helper.DatasetLoader(dataset),
        'debug' : True
    }

    steps = [
        FountainKPMatchings(),
        OpenCVRelativePoseEstimator(),
        TheiaRotationAveraging(),
        TheiaTranslationAveraging(),
        PyTheiaBundleAdjustment(),
        PointCloudVisualizer()
    ]

    gsfm = Pipeline(pipeline_params, steps)
    res = gsfm.run()

    print(res)