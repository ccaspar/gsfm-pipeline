import os
import pickle

import numpy

from intermediateStep import IntermediateStep, InterStepDataMembers, ImagePair, ImageCamera
import numpy as np
import open3d as o3d

class MeshVisualizer(IntermediateStep):
    def __init__(self, options={}, verbose=False):
        super().__init__('MeshVisualizer', options=options, verbose=verbose)


    def getBasisLineSet(self, cameras, factor=0.1, axis=0):
        camera_basis = np.array(
            [cameras[i].globalTranslation + cameras[i].globalRotation[axis, :] * factor for i, _ in enumerate(cameras)])
        camera_positions = np.array([cameras[i].globalTranslation for i, _ in enumerate(cameras)])

        color = [0, 0, 0]
        color[axis] = 1

        colors = [color for i in range(camera_basis.shape[0])]
        indices = [[i, i + camera_basis.shape[0]] for i in range(camera_basis.shape[0])]
        camera_basis = np.concatenate((camera_basis, camera_positions), axis=0)

        line_set = o3d.geometry.LineSet()
        line_set.points = o3d.utility.Vector3dVector(camera_basis)
        line_set.lines = o3d.utility.Vector2iVector(indices)
        line_set.colors = o3d.utility.Vector3dVector(colors)

        return line_set


    def run(self, pipeline_params: dict, run_data: dict):
        # if experiencing problems with pyvista where MESA driver
        # cannot load see https://stackoverflow.com/questions/71010343/cannot-load-swrast-and-iris-drivers-in-fedora-35/72200748#72200748
        elements = []

        if not self.options.get('cameraOnly', False):
            pcd = o3d.geometry.PointCloud()
            pcd.points = o3d.utility.Vector3dVector(run_data['points'])
            pcd.colors = o3d.utility.Vector3dVector(np.ones_like(run_data['points']) * 0.5)


            cl, ind = pcd.remove_radius_outlier(nb_points=4, radius=0.8)
            inlier_cloud = pcd.select_by_index(ind)
            outlier_cloud = pcd.select_by_index(ind, invert=True)
            if self.verbose: print("Showing outliers (red) and inliers (gray): ")
            outlier_cloud.paint_uniform_color([1, 0, 0])
            inlier_cloud.paint_uniform_color([0.2, 0.2, 0.2])



            inlier_cloud.normals = o3d.utility.Vector3dVector(np.zeros((1, 3)))  # invalidate existing normals
            inlier_cloud.estimate_normals()
            inlier_cloud.orient_normals_consistent_tangent_plane(100)

            # generate mesh from the inlier points
            radii = [0.1, 0.2, 0.4, 0.8]
            rec_mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(
                inlier_cloud, o3d.utility.DoubleVector(radii))
            # rec_mesh, densities = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(
            #     pcd, depth=9)
            if self.verbose: print(rec_mesh)
            elements.append(inlier_cloud)
            elements.append(outlier_cloud)
            # elements.append(rec_mesh)

        # points of the cameras
        camera_positions = []
        for cam in run_data[InterStepDataMembers.vertices]:
            if cam.globalTranslation is not None and cam.globalRotation is not None:
                camera_positions.append(cam.globalTranslation)
        pcdCam = o3d.geometry.PointCloud()
        pcdCam.points = o3d.utility.Vector3dVector(camera_positions)
        pcdCam.paint_uniform_color([0, 0, 1])
        elements.append(pcdCam)


        # add view frustums of the cameras
        trans_matricies = []
        for img_cam in run_data[InterStepDataMembers.vertices]:
            if img_cam.globalTranslation is not None and img_cam.globalRotation is not None:
                transformationMatrix = numpy.vstack((numpy.hstack((numpy.transpose(img_cam.globalRotation), np.expand_dims(img_cam.globalTranslation, axis=1))), numpy.array([[0,0,0,1]])))
                trans_matricies.append(transformationMatrix)
                cam = o3d.geometry.LineSet.create_camera_visualization(img_cam.viewSize[0], img_cam.viewSize[1], img_cam.K, numpy.linalg.inv(transformationMatrix), 0.7)
                elements.append(cam)

        if self.options.get('oldCameras', False):
            # load data from old checkpoint
            path = os.path.join('data/checkpoints', 'TheiaTranslationAveraging' + '.cpickle').replace('\\', '/')
            preBundleAdjData = pickle.loads(open(path, "rb").read())
            # draw cameras from before bundle adjust
            camera_positions = np.array([cam.globalTranslation for cam in preBundleAdjData[InterStepDataMembers.vertices]])
            oldpcdCam = o3d.geometry.PointCloud()
            oldpcdCam.points = o3d.utility.Vector3dVector(camera_positions)
            oldpcdCam.paint_uniform_color([0, 0.5, 1])
            elements.append(oldpcdCam)
            # add view frustums of the cameras
            for img_cam in preBundleAdjData[InterStepDataMembers.vertices]:
                transformationMatrix = numpy.vstack((numpy.hstack((numpy.transpose(img_cam.globalRotation), np.expand_dims(img_cam.globalTranslation, axis=1))), numpy.array([[0,0,0,1]])))
                oldcam = o3d.geometry.LineSet.create_camera_visualization(img_cam.viewSize[0], img_cam.viewSize[1], img_cam.K, numpy.linalg.inv(transformationMatrix), 0.5)
                # oldcam.paint_uniform_color([0, 0.5, 1])
                elements.append(oldcam)


        # try to make the view from the cameras position in the middle
        cam_middle_T = trans_matricies[len(trans_matricies)//2]


        front, up, lookat = MeshVisualizer.extract_vectors_from_camera_matrix(cam_middle_T)
        zoom = 0.5
        if self.options.get('cameraOnly', False): zoom = 1
        o3d.visualization.draw_geometries(elements, mesh_show_wireframe=False, mesh_show_back_face=False,
                                          zoom=zoom,
                                          # front=[-0.12457451465734075, -0.95639571618052521, -0.26417498816017471],
                                          front = front,
                                          # lookat=[3.5326351281197632, 1.5228274387026011, -2.511915145878032],
                                          lookat = lookat,
                                          # up=[0.28835707715770814, -0.28965438634108875, 0.91266123645437636])
                                          up = up)
        return run_data

    @staticmethod
    def extract_vectors_from_camera_matrix(camera_transform):
        rotation_matrix = camera_transform[:3, :3]
        translation_vector = camera_transform[:3, 3]

        # Extract the front vector
        front_vector = -rotation_matrix[:, 2]

        # Extract the up vector
        up_vector = -rotation_matrix[:, 1]

        # Extract the lookat vector
        lookat_vector = translation_vector + front_vector

        return front_vector, up_vector, lookat_vector
if __name__ == '__main__':
    print('visualize bundle adjustment from pytheia')

    import helper as helper
    from util.storageStep import LoadIntermediateData
    from pipeline import Pipeline
    from util.storageStep import StoreIntermediateData


    def runEntirePipelineForCheckpoints(data_set):
        from stepKeyPointsMatching.fountainKPMatching import FountainKPMatchings
        from stepRelativePose.openCVRelativePose import OpenCVRelativePoseEstimator
        from stepRotationAveraging.pyTheiaRotationAveraging import TheiaRotationAveraging
        from stepTranslationAveraging.pyTheiaTranslationAveraging import TheiaTranslationAveraging
        from stepBundleAdjustment.pyTheiaBundleAdjustment import PyTheiaBundleAdjustment
        from stepFiltering.pyTheiaRotationFiltering import TheiaRotationFiltering
        from stepFiltering.pyTheiaTranslationFiltering import TheiaTranslationFiltering
        from stepOptimizeTranslation.pyTheiaOptimizeRelativePosition import TheiaOptimizeRelativePosition

        stepsCreateCheckpoints = [
            FountainKPMatchings(),
            OpenCVRelativePoseEstimator(),
            TheiaRotationAveraging(),
            TheiaRotationFiltering(),
            TheiaOptimizeRelativePosition(),
            TheiaTranslationFiltering(),
            TheiaTranslationAveraging(),
            PyTheiaBundleAdjustment(verbose=True),
            StoreIntermediateData('data/checkpoints/reconstruction', 'points'),
        ]
        pipeline_params = Pipeline.getDefaultParams(helper.DatasetLoader(data_set))
        gsfm = Pipeline(pipeline_params, stepsCreateCheckpoints)
        gsfm.addCheckpointsForAll()
        gsfm.removeCheckpoint(type(stepsCreateCheckpoints[8]).__name__)
        return gsfm.run()

    def runVisualizeOnly(data_set):
        pipeline_params = Pipeline.getDefaultParams(helper.DatasetLoader(data_set))

        stepsVisualize = [
            # LoadIntermediateData('data/checkpoints', 'TheiaTranslationAveraging'),
            # MeshVisualizer(True),
            LoadIntermediateData('data/checkpoints/reconstruction', 'points'),
            # PrintIntermediateData(),
            MeshVisualizer({'cameraOnly':True, 'oldCameras':False})
        ]

        gsfm = Pipeline(pipeline_params, stepsVisualize)

        return gsfm.run()

    dataset = 'data/fountain'
    # runEntirePipelineForCheckpoints(data_set=dataset)
    runVisualizeOnly(data_set=dataset)
