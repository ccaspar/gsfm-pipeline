## Example
In the [example.py](example.py) we can see how to use the pipeline with some CSVOverlapMatcher 

To run this you have to download the [kaggle image dataset](https://www.kaggle.com/competitions/image-matching-challenge-2022/data) and update the path in the [example.py](example.py)  

Then you can run
```bash
python src/pipeline/example.py
```