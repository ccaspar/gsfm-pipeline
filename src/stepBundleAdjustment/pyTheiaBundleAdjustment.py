from intermediateStep import IntermediateStep, InterStepDataMembers, ImagePair, ImageCamera
import pytheia as pt

from util.pyTheiaHelper import PyTheiaHelper


class PyTheiaBundleAdjustment(IntermediateStep):
    
    # TODO
    # - add image size to camera and set it for the views (urgent + other camera intrisincs)
    # - change loss in BA options 
    # - change set_camera_poition prior in BA options
    # - bundle adjust camera rotations/positions
    # - select/filter good/bad rotations and positions (sebastian?)
    # - draw camera positions/orientations in BA result
    # - constant camera pos and rot in BA options (what is set in the code exactly????)
    # - store image pixel color in camera so that we can display image color in reconstruction (urgent to have a better idea)
    # - center data around it's centroid (quite urgent)
    # - filtering of ill defined vertices in connected components (rethink the method)
    # - check CameraIntrinsicsGroupIdFromViewId on recon to see what it is exactly
    # - view should has v.HasPositionPrior() == True ????

    @staticmethod
    def getDefaultOptions(verbose, min_observations=8):
        options = pt.sfm.BundleAdjustmentOptions()
        #options.function_tolerance = math.inf
        #options.gradient_tolerance = math.inf
        options.loss_function_type = pt.sfm.LossFunctionType.HUBER
        #options.intrinsics_to_optimize = pt.sfm.OptimizeIntrinsicsType.ALL
        options.verbose = verbose
        #options.constant_camera_orientation = True
        #options.constant_camera_position = True
        options.max_num_iterations = 10

        return {
            'pythiaOptions' : options,
            'min_observations' : min_observations
        }

    @staticmethod
    def SetUnderconstrainedAsUnestimated(recon: pt.sfm.Reconstruction):
        num_underconstrained_views = -1;
        num_underconstrained_tracks = -1;
        while num_underconstrained_views != 0 and num_underconstrained_tracks != 0:
            num_underconstrained_views = pt.sfm.SetUnderconstrainedViewsToUnestimated(recon)
            num_underconstrained_tracks = pt.sfm.SetUnderconstrainedTracksToUnestimated(recon)

    @staticmethod
    def GetReconstructionOptions() -> pt.sfm.ReconstructionEstimatorOptions:
        recon_options = pt.sfm.ReconstructionEstimatorOptions()
        recon_options.num_threads = 4
        recon_options.rotation_filtering_max_difference_degrees = 10.0
        recon_options.bundle_adjustment_robust_loss_width = 3.0
        recon_options.bundle_adjustment_loss_function_type = pt.sfm.LossFunctionType(1)
        recon_options.subsample_tracks_for_bundle_adjustment = True
        recon_options.filter_relative_translations_with_1dsfm = True

        return recon_options

    @staticmethod
    def SetBundleAdjustmentOptions(recon_options: pt.sfm.ReconstructionEstimatorOptions) -> pt.sfm.BundleAdjustmentOptions:
        ba_options = pt.sfm.BundleAdjustmentOptions()
        ba_options.num_threads = recon_options.num_threads
        ba_options.loss_function_type = recon_options.bundle_adjustment_loss_function_type
        ba_options.robust_loss_width = recon_options.bundle_adjustment_robust_loss_width
        ba_options.use_inner_iterations = True
        ba_options.intrinsics_to_optimize = recon_options.intrinsics_to_optimize
        return ba_options

    def __init__(self, options=None, verbose=False, triangulate_only=False):
        super().__init__('pyTheiaBundleAdjustment', verbose=verbose)
        if options is None:
            options = self.getDefaultOptions(verbose)
        self.triangulate_only = triangulate_only
        self.options = options

    def run(self, pipeline_params: dict, run_data: dict):
        recon = PyTheiaHelper.create_recon_from_data(run_data)

        views_ids = recon.ViewIds()

        track_builder = PyTheiaHelper.create_tracks_from_data(run_data)
        track_builder.BuildTracks(recon)

        if self.verbose:
            print(f'computing tracks...')
        for vid in views_ids:
            view = recon.MutableView(vid)
            view.SetIsEstimated(True)

        track_options = pt.sfm.TrackEstimatorOptions()
        track_options.max_acceptable_reprojection_error_pixels = 10.0
        track_options.min_triangulation_angle_degrees = 3.0
        track_options.bundle_adjustment = True
        track_options.num_threads = 4
        track_options.triangulation_method = pt.sfm.TriangulationMethodType.MIDPOINT
        track_estimator = pt.sfm.TrackEstimator(track_options, recon)
        track_estimator.EstimateAllTracks()

        PyTheiaBundleAdjustment.SetUnderconstrainedAsUnestimated(recon)


        recon_options = PyTheiaBundleAdjustment.GetReconstructionOptions()
        ba_options = PyTheiaBundleAdjustment.SetBundleAdjustmentOptions(recon_options)

        if self.verbose:
            for track in recon.TrackIds():
                print(f"Track {track} is estimated: {recon.Track(track).IsEstimated()}")
            for view in recon.ViewIds():
                print(f"Track {view} is estimated: {recon.View(view).IsEstimated()}")


        tracks_to_optimize = set()
        views_to_optimize = set()

        for track in recon.TrackIds():
            if recon.Track(track).IsEstimated():
                tracks_to_optimize.add(track)

        for view in recon.ViewIds():
            if recon.View(view).IsEstimated():
                views_to_optimize.add(view)


        ba_summary: pt.sfm.BundleAdjustmentSummary = pt.sfm.BundleAdjustPartialReconstruction(ba_options,views_to_optimize, tracks_to_optimize, recon)
        num_points_removed = pt.sfm.SetOutlierTracksToUnestimated(tracks_to_optimize, recon_options.max_reprojection_error_in_pixels, recon_options.min_triangulation_angle_degrees, recon)
        if self.verbose:
            print(f"Bundle adjustment successful: {ba_summary.success}")
            print(f"Outlier removal through bundle adjustment: {num_points_removed}")

        bundled_adjusted_points = []
        track_ids = recon.TrackIds()
        for tid in track_ids:
            mutable_track = recon.MutableTrack(tid)
            point4D = mutable_track.Point()
            if point4D[-1] == 0:
                continue
            point3D = point4D[:-1] / point4D[-1]
            bundled_adjusted_points.append(point3D)

        run_data[InterStepDataMembers.reconstruction] = bundled_adjusted_points

        return run_data



if __name__ == '__main__':

    print('running bundle ajuster from pytheia')
    
    import helper as helper
    from pipeline import Pipeline

    dataset = 'data/fountain'

    pipeline_params = {
        'image_loader': helper.DatasetLoader(dataset),
        'verbose': True
    }

    from stepKeyPointsMatching.fountainKPMatching import FountainKPMatchings
    from stepKeyPointsMatching.openCVKeyPointsMatching import OpenCVKeyPointsMatcher
    from stepImageOverlap.resNetGemPoolOverlap import ResNetGemPoolOverlap

    from stepRelativePose.openCVRelativePose import OpenCVRelativePoseEstimator
    from stepRelativePose.pyTheiaRelativePose import PyTheiaRelativePose

    from stepRotationAveraging.pyTheiaRotationAveraging import PyTheiaRotationAveraging
    from stepFiltering.pyTheiaRotationFiltering import PyTheiaRotationFiltering
    from stepOptimizeTranslation.pyTheiaOptimizeRelativePosition import PyTheiaOptimizeRelativePosition
    from stepFiltering.pyTheiaTranslationFiltering import PyTheiaTranslationFiltering
    from stepTranslationAveraging.pyTheiaTranslationAveraging import PyTheiaTranslationAveraging
    from stepVisualize.meshVisualize import MeshVisualizer

    steps = [
        ResNetGemPoolOverlap(),
        OpenCVKeyPointsMatcher(),
        # FountainKPMatchings(),
        #OpenCVKeyPointsMatcher(),
        PyTheiaRelativePose(),
        # OpenCVRelativePoseEstimator(),
        PyTheiaRotationAveraging(),
        PyTheiaRotationFiltering(verbose=True),
        PyTheiaOptimizeRelativePosition(verbose=True),
        PyTheiaTranslationFiltering(verbose=True),
        PyTheiaTranslationAveraging(verbose=True),
        PyTheiaBundleAdjustment(verbose=False),
        MeshVisualizer(),
    ]

    gsfm = Pipeline(pipeline_params, steps)
    # gsfm.addCheckpointsForAll()
    # gsfm.removeCheckpoint("MeshVisualizer")

    gsfm.addCheckpoint("ResNetGemPoolOverlap")
    gsfm.addCheckpoint("OpenCVKeyPointsMatcher")
    gsfm.addCheckpoint("PyTheiaRelativePose")
    gsfm.addCheckpoint("PyTheiaRotationAveraging")

    # gsfm.addCheckpointsForAll()
    # gsfm.removeCheckpoint("PyTheiaBundleAdjustment")
    # gsfm.removeCheckpoint("MeshVisualizer")
    res = gsfm.run()
