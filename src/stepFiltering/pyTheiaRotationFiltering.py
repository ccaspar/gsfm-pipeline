import pytheia as pt
from scipy.spatial.transform import Rotation as R

from intermediateStep import IntermediateStep, InterStepDataMembers



class PyTheiaRotationFiltering(IntermediateStep):

    def __init__(self, verbose=False):
        super().__init__('pyTheiaRotationFiltering', verbose=verbose)
        pass

    def run(self, pipeline_params: dict, run_data: dict):
        # sets the filter for rotation difference when angles should be removed
        rotation_filtering_max_difference_degrees = 10
        rotations = {}

        # build view graph
        view_graph = pt.sfm.ViewGraph()
        for imagePair in run_data[InterStepDataMembers.edges]:

            imagePair.rotationOutlier = True
            id1 = imagePair.image1.index
            id2 = imagePair.image2.index
            two_view_info = pt.sfm.TwoViewInfo()

            rotations[id1] = R.from_matrix(imagePair.image1.globalRotation).as_rotvec()
            rotations[id2] = R.from_matrix(imagePair.image2.globalRotation).as_rotvec()
            two_view_info.focal_length_1 = imagePair.image1.K[0][0]
            two_view_info.focal_length_2 = imagePair.image2.K[0][0]
            two_view_info.position_2 = imagePair.t
            two_view_info.rotation_2 = imagePair.R
            two_view_info.visibility_score = 0
            view_graph.AddEdge(id1, id2, two_view_info)

        all_edges = len(view_graph.GetAllEdges())
        pt.sfm.FilterViewPairsFromOrientation(rotations, rotation_filtering_max_difference_degrees,view_graph)
        removed_rotation_ids = pt.sfm.RemoveDisconnectedViewPairs(view_graph)

        for view_id in removed_rotation_ids:
            del run_data[InterStepDataMembers.vertices][view_id]


        good_edge_count = len(view_graph.GetAllEdges())
        new_image_pairs = []
        for good_edges in view_graph.GetAllEdges():
            id0 = good_edges[0]
            id1 = good_edges[1]
            for imagePair in run_data[InterStepDataMembers.edges]:
                if imagePair.image1.index == id0 and imagePair.image2.index == id1:
                    imagePair.rotationOutlier = False
                    new_image_pairs.append(imagePair)

        run_data[InterStepDataMembers.edges] = new_image_pairs
        if self.verbose:
            print(f"Edge Count: {all_edges} Good Edges: {good_edge_count} Bad Edges: {all_edges - good_edge_count}")

        return run_data

if __name__ == '__main__':
    print("test rotation filtering")

    from src import helper
    from pipeline import Pipeline
    from stepKeyPointsMatching.fountainKPMatching import FountainKPMatchings
    from stepRelativePose.pyTheiaRelativePose import PyTheiaRelativePose
    from stepRotationAveraging.pyTheiaRotationAveraging import PyTheiaRotationAveraging

    folder_name = "data/fountain"
    pipeline_params = {
        'image_loader': helper.DatasetLoader(folder_name)
    }

    steps = [
        FountainKPMatchings(),
        PyTheiaRelativePose(),
        PyTheiaRotationAveraging(),
        PyTheiaRotationFiltering(verbose=True)
    ]

    gsfm = Pipeline(pipeline_params, steps)
    res = gsfm.run()