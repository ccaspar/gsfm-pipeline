import numpy as np
from typing import List

import pytheia as pt
from scipy.spatial.transform import Rotation as R

from intermediateStep import IntermediateStep, InterStepDataMembers
from util.pyTheiaHelper import PyTheiaHelper


class PyTheiaOptimizeRelativePosition(IntermediateStep):

    def __init__(self, verbose=False):
        super().__init__('pyTheiaOptimizeRelativePosition', verbose=verbose)

    def GetNormalizedFeatureCorrespondences(self, view1: pt.sfm.View, view2: pt.sfm.View, correspondences: List[pt.matching.FeatureCorrespondence]):
        camera1: pt.sfm.Camera = view1.Camera()
        camera2: pt.sfm.Camera = view2.Camera()

        track_ids = view1.TrackIds()
        for track_id in track_ids:
            feature2 = view2.GetFeature(track_id)

            if feature2 is None:
                continue

            match = pt.matching.FeatureCorrespondence()
            feature1 = view1.GetFeature(track_id)
            match.feature1 = feature1
            match.feature2 = feature2

            tmp1 = camera1.PixelToNormalizedCoordinates(match.feature1.point)
            tmp2 = camera2.PixelToNormalizedCoordinates(match.feature2.point)

            # this is hnormalize from eigen https://eigen.tuxfamily.org/dox/group__Geometry__Module.html#gadc0e3dd3510cb5a6e70aca9aab1cbf19
            match.feature1.point = tmp1[:-1]/tmp1[-1]
            match.feature2.point = tmp2[:-1]/tmp2[-1]
            correspondences.append(match)

    def run(self, pipeline_params: dict, run_data: dict):
        if "debug" in pipeline_params:
            save_translations = []
            new_translations = []

        recon = PyTheiaHelper.create_recon_from_data(run_data)
        track_builder = PyTheiaHelper.create_tracks_from_data(run_data)
        track_builder.BuildTracks(recon)

        for imagePair in run_data[InterStepDataMembers.edges]:
            id1 = imagePair.image1.index
            id2 = imagePair.image2.index

            rotation1 = R.from_matrix(imagePair.image1.globalRotation).as_rotvec()
            rotation2 = R.from_matrix(imagePair.image2.globalRotation).as_rotvec()

            view1 = recon.View(id1)
            view2 = recon.View(id2)
            matches = []

            self.GetNormalizedFeatureCorrespondences(view1, view2, matches)

            # update relative position
            result = pt.sfm.OptimizeRelativePositionWithKnownRotation(matches, rotation1, rotation2)
            if result[0]:
                imagePair.t = result[1]
            if "debug" in pipeline_params:
                new_translations.append(imagePair.t)

        if "debug" in pipeline_params:
            count = 0
            for i in range(len(save_translations)):
                if not np.array_equal(save_translations[i], new_translations[i]):
                    count += 1
            print(f"Translation_Count: {len(save_translations)}, translations changed: {count}")
        return run_data