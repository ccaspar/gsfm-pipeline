import cv2
import numpy as np

from intermediateStep import IntermediateStep, InterStepDataMembers
from scipy.spatial.transform import Rotation

"""
References:

Stack Threads for relative Pose:
https://robotics.stackexchange.com/questions/14456/determine-the-relative-camera-pose-given-two-rgb-camera-frames-in-opencv-python
https://stackoverflow.com/questions/13376441/relative-camera-pose-estimation-using-opencv


OpenCV Find Essential Matrix:
https://docs.opencv.org/3.0-beta/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#findessentialmat

OpenCV Recover Pose:
https://docs.opencv.org/3.0-beta/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#recoverpose

Get Picture Coordinates from CV2.Dmatch:
https://stackoverflow.com/questions/54203873/how-to-get-the-positions-of-the-matched-points-with-brute-force-matching-sift/54220651#54220651


"""


class OpenCVRelativePoseEstimator(IntermediateStep):

    def __init__(self, params=None):
        super().__init__('OpenCVRelativePoseEstimator')
        if params is None:
            params = {'debug': False}
        self.params = params

    def run(self, pipeline_params: dict, run_data: dict):
        """
        Estimates Relative Pose between two images given intrinsic parameters and matched keypoints
        Important: Assumes both pictures taken with same camera
        Arg:
            matches: matched features from feature matcher (as cv2.DMatch object)
            debug: if set to true prints intermediate infos

        Returns:
            dict : {
                R: Relative rotation between images (np.array (3,3))
                t: Relative translation between images (np.array (3,1))
            }
        """

        for index, image_pair in enumerate(run_data[InterStepDataMembers.edges]):

            # extract (x,y) out of DMatch object
            p1 = []
            p2 = []

            # p1 and p2 are lists of the (x,y) coordinates in img1/img2 respectively of coordinates
            for match in image_pair.matchings:
                p1.append(image_pair.image1.keyPoints[match[0]])
                p2.append(image_pair.image2.keyPoints[match[1]])

            p1 = np.asarray(p1)
            p2 = np.asarray(p2)

            # Use this to undistort: https://stackoverflow.com/questions/33906111/how-do-i-estimate-positions-of-two-cameras-in-opencv
            # Normalize for Essential Matrix calculation
            p1_norm = cv2.undistortPoints(np.expand_dims(p1, axis=1), cameraMatrix=image_pair.image1.K, distCoeffs=None)
            p2_norm = cv2.undistortPoints(np.expand_dims(p2, axis=1), cameraMatrix=image_pair.image2.K, distCoeffs=None)

            threshold = 1.5 / max(image_pair.image1.K[0,0], image_pair.image1.K[1,1], image_pair.image2.K[0,0], image_pair.image2.K[1,1])

            # calculate Essential Matrix and recover pose
            essentialMatrix, mask = cv2.findEssentialMat(p1_norm, p2_norm, focal=1.0, pp=(0., 0.), method=cv2.RANSAC, prob=0.999, threshold=threshold)
            inlierCount, rotationMatrix, translationVector, mask = cv2.recoverPose(essentialMatrix, p1_norm, p2_norm)

            if self.params['debug']:
                print(image_pair.image1.img + " vs " + image_pair.image2.img)
                print("Essential Matrix: \n", essentialMatrix)
                print("Inlier Count: ", inlierCount)
                print("Rotation Matrix Shape: ", rotationMatrix.shape)
                print("Rotation Matrix: \n", rotationMatrix)
                print("Translation Vector Shape: ", translationVector.shape)
                print("Translation Vector: \n", translationVector)
            
            image_pair.E = essentialMatrix
            image_pair.R = Rotation.from_matrix(rotationMatrix.T).as_rotvec()
            image_pair.t = translationVector

        return run_data
