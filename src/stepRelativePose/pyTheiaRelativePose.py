from typing import Tuple
import numpy as np
from intermediateStep import ImagePair, ImageCamera
import pytheia as pt
import sys
from scipy.spatial.transform import Rotation as R
import copy

from intermediateStep import IntermediateStep, InterStepDataMembers
from util.pyTheiaHelper import PyTheiaHelper


def correspondence_from_indexed_matches(filtered_matches, pts1, pts2):
    correspondences = []
    for match in filtered_matches:
        point1 = np.array(pts1[match.queryIdx])
        point2 = np.array(pts2[match.trainIdx])
        feature_correspondece = pt.matching.FeatureCorrespondence(
            pt.sfm.Feature(point1), pt.sfm.Feature(point2))
        correspondences.append(feature_correspondece)

    return correspondences

class PyTheiaRelativePose(IntermediateStep):
    def __init__(self, verbose=False):
        super().__init__('PyTheiaRelativePose', verbose=verbose)
        self.verbose = verbose


    def GetTwoViewInfo(self, image_pair: ImagePair, options, prior) -> Tuple[bool, pt.sfm.TwoViewInfo]:
        correspondences = []
        for match in image_pair.matchings:
            point1 = np.array(image_pair.image1.keyPoints[match[0]])
            point2 = np.array(image_pair.image2.keyPoints[match[1]])
            feature_correspondence = pt.matching.FeatureCorrespondence(
                pt.sfm.Feature(point1), pt.sfm.Feature(point2))
            correspondences.append(feature_correspondence)

        success, twoview_info, inlier_indices = pt.sfm.EstimateTwoViewInfo(
            options, prior, prior, correspondences)

        # TODO change this magic number to a parameter
        min_num_inlier_matches = 30
        if len(inlier_indices) < min_num_inlier_matches:
            print('Number of putative matches after geometric verification are too low!')
            return False, None
        else:
            verified_matches = []
            for i in range(len(inlier_indices)):
                verified_matches.append(image_pair.matchings[inlier_indices[i]])

            correspondences = []
            simple_correspondences = []
            for match in verified_matches:
                point1 = np.array(image_pair.image1.keyPoints[match[0]])
                point2 = np.array(image_pair.image2.keyPoints[match[1]])
                feature_correspondence = pt.matching.FeatureCorrespondence(
                    pt.sfm.Feature(point1), pt.sfm.Feature(point2))
                correspondences.append(feature_correspondence)
                simple_correspondences.append((point1, point2))

            image_pair.correspondences = simple_correspondences
            image_pair.matchings = verified_matches

            twoview_info.num_verified_matches = len(verified_matches)

        return success, twoview_info

    def run(self, pipeline_params: dict, run_data: dict):
        options = pt.sfm.EstimateTwoViewInfoOptions()
        options.ransac_type = pt.sfm.RansacType(0)
        options.use_lo = True
        options.use_mle = True

        new_image_pairs = []
        for index, image_pair in enumerate(run_data[InterStepDataMembers.edges]):
            prior = PyTheiaHelper.create_prior_from_cam(image_pair.image1)
            success, two_view_info = self.GetTwoViewInfo(image_pair, options, prior)

            if success:
                image_pair.t = two_view_info.position_2
                image_pair.R = two_view_info.rotation_2
                new_image_pairs.append(image_pair)
            elif self.verbose:
                print("No match between image {} and image {}. ".format(image_pair.image1.index, image_pair.image2.index))


        run_data[InterStepDataMembers.edges] = new_image_pairs
        return run_data
