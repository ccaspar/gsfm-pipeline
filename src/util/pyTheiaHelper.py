from intermediateStep import ImageCamera, InterStepDataMembers, ImagePair
import pytheia as pt
import sys

class PyTheiaHelper:

    @staticmethod
    def create_prior_from_cam(cam: ImageCamera)-> pt.sfm.CameraIntrinsicsPrior:
        K = cam.K
        prior = pt.sfm.CameraIntrinsicsPrior()
        prior.focal_length.value = [K[0, 0]]
        prior.aspect_ratio.value = [K[1, 1] / K[0, 0]]
        prior.principal_point.value = [K[0, 2], K[1, 2]]
        prior.image_width = cam.width
        prior.image_height = cam.height

        # TODO set correct skew
        prior.skew.value = [0]
        prior.camera_intrinsics_model_type = 'PINHOLE'

        return prior
    @staticmethod
    def create_recon_from_data(run_data: dict, verbose=False) -> pt.sfm.Reconstruction:
        cameras: list[ImageCamera] = run_data[InterStepDataMembers.vertices]

        recon = pt.sfm.Reconstruction()

        # the view ids a created and
        views_ids = [recon.AddView(f'v{c.index}', 0, c.index) for _, c in enumerate(cameras)]

        # query views (object) for the whole reconstruction (the view need to have been added by AddView method)
        mutable_views = [recon.MutableView(id) for id in views_ids]  # Mutable just stands for non-const in c++
        mutable_cameras = [view.MutableCamera() for view in mutable_views]

        for i, cam in enumerate(cameras):
            if cam.globalTranslation is not None:
                mutable_cameras[i].SetPosition(cam.globalTranslation)
            if cam.globalRotation is not None:
                mutable_cameras[i].SetOrientationFromRotationMatrix(cam.globalRotation)
            prior = PyTheiaHelper.create_prior_from_cam(cam)
            mutable_views[i].SetCameraIntrinsicsPrior(prior)

        pt.sfm.SetCameraIntrinsicsFromPriors(recon)

        if verbose:
            for i, cam in enumerate(cameras):
                mutable_cameras[i].PrintCameraIntrinsics()
                # print also to stderr so that intrinsics match with the images size
                print(f"Image Width: {mutable_views[i].Camera().ImageWidth()}", file=sys.stderr)
                print(f"Image Height: {mutable_views[i].Camera().ImageHeight()}", file=sys.stderr)

        return recon

    @staticmethod
    def create_tracks_from_data(run_data: dict, min_track_length=3, max_track_length=30, verbose=False) -> pt.sfm.TrackBuilder:
        track_builder = pt.sfm.TrackBuilder(min_track_length, max_track_length)

        for cam in run_data[InterStepDataMembers.edges]:
            for correspondence in cam.correspondences:
                feature1 = pt.sfm.Feature(correspondence[0])
                feature2 = pt.sfm.Feature(correspondence[1])
                track_builder.AddFeatureCorrespondence(cam.image1.index, feature1, cam.image2.index, feature2)

        return track_builder





