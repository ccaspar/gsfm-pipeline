import pickle
import os

from intermediateStep import IntermediateStep


class StoreIntermediateData(IntermediateStep):
    """
    Args:
        folder: The folder where the data will be stored (does not need to exist)
        filename: The name of the file without extension
        Intermediate step that stores the data dict input in a pickle dump
    """

    def __init__(self, folder, filename):
        super().__init__('SaveData')
        self.folder = folder
        if not os.path.exists(self.folder):
            os.mkdir(self.folder)
        self.filename = filename

    def run(self, pipeline_params: dict, run_data: dict) -> dict:
        path = os.path.join(self.folder, self.filename + '.cpickle').replace('\\', '/')
        f = open(path, "wb")
        f.write(pickle.dumps(run_data))
        f.close()
        print('Stored input to: ' + path)

        return run_data

    def get_path(self) -> str:
        return os.path.join(self.folder, self.filename + '.cpickle').replace('\\', '/')

class LoadIntermediateData(IntermediateStep):
    """
    Args:
        folder: The folder where the data file is located
        filename: The name of the file without extension
        Intermediate step that loads the data dict from a pickle dump
    """

    def __init__(self, folder, filename):
        super().__init__('LoadData')
        self.folder = folder
        self.filename = filename

    def run(self, pipeline_params: dict, run_data: dict) -> dict:
        path = os.path.join(self.folder, self.filename + '.cpickle').replace('\\', '/')
        res = pickle.loads(open(path, "rb").read())
        # print('Loaded: ', res)
        return res

    def get_path(self) -> str:
        return os.path.join(self.folder, self.filename + '.cpickle').replace('\\', '/')