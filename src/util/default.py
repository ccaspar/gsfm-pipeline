
from util.printStep import PrintIntermediateData
from stepImageOverlap.resNetGemPoolOverlap import ResNetGemPoolOverlap
from stepKeyPointsMatching.openCVKeyPointsMatching import OpenCVKeyPointsMatcher
from stepKeyPointsMatching.fountainKPMatching import FountainKPMatchings
from stepRelativePose.pyTheiaRelativePose import PyTheiaRelativePose
from stepRotationAveraging.pyTheiaRotationAveraging import PyTheiaRotationAveraging
from stepTranslationAveraging.pyTheiaTranslationAveraging import PyTheiaTranslationAveraging
from stepBundleAdjustment.pyTheiaBundleAdjustment import PyTheiaBundleAdjustment
from stepFiltering.pyTheiaRotationFiltering import PyTheiaRotationFiltering
from stepFiltering.pyTheiaTranslationFiltering import PyTheiaTranslationFiltering
from stepOptimizeTranslation.pyTheiaOptimizeRelativePosition import PyTheiaOptimizeRelativePosition

from stepVisualize.meshVisualize import MeshVisualizer

import pathlib
from pipeline import Pipeline
import helper as helper
from util.storageStep import StoreIntermediateData

class DefaultSteps:

    steps = [
        ResNetGemPoolOverlap(),
        OpenCVKeyPointsMatcher(),
        PyTheiaRelativePose(),
        PyTheiaRotationAveraging(),
        PyTheiaRotationFiltering(),
        PyTheiaOptimizeRelativePosition(),
        PyTheiaTranslationFiltering(),
        PyTheiaTranslationAveraging(),
        PyTheiaBundleAdjustment(),
        MeshVisualizer()
    ]

    """
    Returns the default steps for the whole pipeline
    """
    @staticmethod
    def get(until:int=len(steps)):
        """
        Arg:
            until: integer index (not included) of the step
            if None then all the steps are returned
        Returns:
            array of Intermediate steps (without prints)
        """
        return DefaultSteps.steps[:until]
        
    @staticmethod
    def makeCheckPoint(dataset_folder:str, checkpoint_folder:str, checkpoint_filename:str, until:int=len(steps)):
        """
        Run the pipeline until the step specified and creates
        a checkpoint in the specified folder. If the file checkpoint_file
        already exists at the checkpoint_folder location this function does nothing.
        If the checkpoint_file does not exists at the checkpoint_folder then this function
        runs the pipeline.
        
        Args:
        - datasetfilder: str path to the dataset to be used for the checkpoint
        - checkpoint_folder: str path to the folder of checkpoint
        - checkpoint_filename: str filename of the checkpoint (no extension!)
        - until (Optional): int the step until we want to run
        
        Returns:
        - None (the checkpoint may then be loaded using the Load step)

        """
        steps_until = DefaultSteps.get(until)

        pipeline_params = {
            'image_loader': helper.DatasetLoader(dataset_folder)
        }

        checkpoint_folder_path = pathlib.Path(checkpoint_folder).resolve()
        if not checkpoint_folder_path.exists():
            print(f'creating checkpoint folder at {str(checkpoint_folder_path)}')
            checkpoint_folder_path.mkdir()

        checkpoint_path = checkpoint_folder_path / (checkpoint_filename + '.cpickle')
        if not checkpoint_path.exists():
            steps_until.append(StoreIntermediateData(str(checkpoint_folder_path), checkpoint_filename))
            gsfm = Pipeline(pipeline_params, steps_until)
            gsfm.run()
            print(f'finished running Pipeline for checkpoint {checkpoint_filename}')
        else:
            print(f'checkpoint already exists at {checkpoint_path}')
