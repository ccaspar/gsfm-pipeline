from intermediateStep import IntermediateStep
from pprint import pprint

class PrintIntermediateData(IntermediateStep):
    """
    Args:
        folder: The folder where the data will be stored (does not need to exist)
        filename: The name of the file without extension
        Intermediate step that stores the data dict input in a pickle dump
    """

    def __init__(self):
        super().__init__('Printing Data')

    def run(self, pipeline_params: dict, run_data: dict) -> dict:
        pprint(run_data)
        return run_data
