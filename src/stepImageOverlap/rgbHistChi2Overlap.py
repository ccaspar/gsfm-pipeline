import pickle

import numpy as np
import cv2
from PIL import Image

import helper
from intermediateStep import IntermediateStep, InterStepDataMembers, ImagePair, ImageCamera


class RGBHistogram:
    def __init__(self, bins):
        # store the number of bins the histogram will use
        self.bins = bins

    def describe(self, image):
        # compute a 3D histogram in the RGB colorspace,
        # then normalize the histogram so that images
        # with the same content, but either scaled larger
        # or smaller will have (roughly) the same histogram

        # Quentin: here input image is PIL and caclHist crashes 13.04.23 (something got modified, before the image was opencv I think)
        # fix: convert image to opencv friendly format from PIL, numpy then RGB -> BGR
        image_np = np.array(image)[:, :, ::-1]
        hist = cv2.calcHist([image_np], [0, 1, 2],
                            None, self.bins, [0, 256, 0, 256, 0, 256])
        # normalize with OpenCV 2.4
        hist = cv2.normalize(hist, hist)
        # return out 3D histogram as a flattened array
        return hist.flatten()


class RGBHistChi2Overlap(IntermediateStep):
    def __init__(self, histogramBins=None):  # , images_set_path="data/capitole"):
        super().__init__('ImageOverlap')

        if histogramBins is None:
            histogramBins = [8, 8, 8]
        self.desc = RGBHistogram(histogramBins)
        self.dist = self.chi2_distance
        self.imgs = []

    # def loadIndex(self, index_path="data/index.cpickle"):
    #     self.index = pickle.loads(open(index_path, "rb").read())
    #
    # def saveIndex(self, index_path="data/index.cpickle"):
    #     f = open(index_path, "wb")
    #     f.write(pickle.dumps(self.index))
    #     f.close()

    def setDescriptorArgs(self, **args):
        self.desc = RGBHistogram(args)

    def calcIndex(self, loader: helper.DatasetLoader):
        self.imgs = loader.images
        for imageCamera in self.imgs:  # list_images(self.dataset_path):
            # extract our unique image ID (i.e. the filename)

            image = Image.open(imageCamera.path)
            features = self.desc.describe(image)
            image.close()
            imageCamera.globalFeatures = features
        print("[INFO] done...indexed {} images".format(len(self.imgs)))

    def search(self, query: ImageCamera):
        # initialize our dictionary of results
        results = {}
        # loop over the index
        for i, img in enumerate(self.imgs):
            if query == img:
                continue

            d = self.dist(img.globalFeatures, query.globalFeatures)

            results[i] = d

        results = sorted(results.items(), key=lambda x: x[1])
        return results

    def chi2_distance(self, histA, histB, eps=1e-10):
        # compute the chi-squared distance
        d = 0.5 * np.sum([((a - b) ** 2) / (a + b + eps)
                          for (a, b) in zip(histA, histB)])

        return d

    def createPairs(self, threshold=0.4):
        imagePairs = []
        adjacencyList = []
        for i in range(len(self.imgs)):
            adjacencyList.append([])

        for i, query in enumerate(self.imgs):
            for (k, v) in self.search(query):
                if v > threshold:
                    continue
                if k in adjacencyList[i] or i in adjacencyList[k]:
                    continue
                adjacencyList[i].append(k)
                adjacencyList[k].append(i)
                imagePairs.append(ImagePair(query, self.imgs[k], v))
        return imagePairs, adjacencyList

    def run(self, pipeline_params: dict, run_data: dict) -> dict:
        self.calcIndex(pipeline_params['image_loader'])
        (imagePairs, adjacencyList) = self.createPairs()
        return {
            InterStepDataMembers.vertices: [i.clearFeatureCache() for i in self.imgs],
            InterStepDataMembers.edges: imagePairs,
            InterStepDataMembers.adjacencyList: adjacencyList
        }

