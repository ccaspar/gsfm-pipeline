import numpy as np
import sys
import os

from intermediateStep import IntermediateStep, InterStepDataMembers, ImagePair

import torch
from torchvision import transforms

sys.path.append("src/packages/cnnimageretrieval-pytorch/")

from cirtorch.networks.imageretrievalnet import init_network, extract_vectors
from cirtorch.datasets.datahelpers import cid2filename
from cirtorch.datasets.testdataset import configdataset

from cirtorch.utils.download import download_train, download_test
from cirtorch.utils.whiten import whitenlearn, whitenapply
from cirtorch.utils.evaluate import compute_map_and_print
from cirtorch.utils.general import get_data_root, htime


class ResNetGemPoolOverlap(IntermediateStep):
    def __init__(self, image_size=1024, multiscale="[1, 1/2**(1/2), 1/2]", whitening="retrieval-SfM-120k", verbose=False):
        super().__init__('ImageOverlap', verbose=verbose)
        # if not torch.cuda.is_available():
        #     raise EnvironmentError("CUDA not installed")

        self.network_path = "data/checkpoints/networks/retrievalSfM120k-resnet101-gem-b80fb85.pth"
        download_net(self.network_path, self.verbose)
        self.image_size = image_size
        self.multiscale = multiscale
        self.whitening = whitening
        self.net = None
        self.load_network()
        self.imgs = []

    def load_network(self):
        state = torch.load(self.network_path)

        net_params = {'architecture': state['meta']['architecture'], 'pooling': state['meta']['pooling'],
                      'local_whitening': state['meta'].get('local_whitening', False),
                      'regional': state['meta'].get('regional', False),
                      'whitening': state['meta'].get('whitening', False), 'mean': state['meta']['mean'],
                      'std': state['meta']['std'], 'pretrained': False}

        # load network
        net = init_network(net_params)
        net.load_state_dict(state['state_dict'])

        # if whitening is precomputed
        if 'Lw' in state['meta']:
            net.meta['Lw'] = state['meta']['Lw']
        if self.verbose:
            print(">>>> loaded network: ")
        # print(net.meta_repr())

        self.net = net

    def run(self, pipeline_params: dict, run_data: dict) -> dict:
        self.imgs = pipeline_params['image_loader'].images
        image_list = [i.path for i in self.imgs]
        images = [i.img for i in self.imgs]

        descriptor = self.get_descriptor(image_list)
        adj_list = self.get_matches(descriptor, descriptor, images)
        pair_list, new_adj_list = self.genearate_pairs(adj_list, dict(zip(images, image_list)))
        return {
            InterStepDataMembers.vertices: [i.clearFeatureCache() for i in self.imgs],
            InterStepDataMembers.edges: pair_list,
            InterStepDataMembers.adjacencyList: new_adj_list
        }

    def genearate_pairs(self, adj_list, paths):
        pairs = [ImagePair(next((x for x in self.imgs if x.img == key), None), next((x for x in self.imgs if x.img == value), None)) for key in adj_list
                 for value in adj_list[key]]
        res_list = []
        new_adj_list = []
        for i in range(len(self.imgs)):
            new_adj_list.append([])

        for item in pairs:
            if item not in res_list:
                res_list.append(item)
                new_adj_list[item.image1.index].append(item.image2.index)
                new_adj_list[item.image2.index].append(item.image1.index)

        return res_list, new_adj_list

    def get_descriptor(self, image_list):
        net = self.net
        ms = list(eval(self.multiscale))
        if len(ms) > 1 and net.meta['pooling'] == 'gem' and not net.meta['regional'] and not net.meta['whitening']:
            msp = net.pool.p.item()
        else:
            msp = 1
        if torch.cuda.is_available():
            net.cuda()
        net.eval()

        # set up the transform
        normalize = transforms.Normalize(
            mean=net.meta['mean'],
            std=net.meta['std']
        )
        transform = transforms.Compose([
            transforms.ToTensor(),
            normalize
        ])

        if self.whitening is not None:

            if 'Lw' in net.meta and self.whitening in net.meta['Lw']:
                if self.verbose:
                    print('>> {}: Whitening is precomputed, loading it...'.format(self.whitening))

                if len(ms) > 1:
                    Lw = net.meta['Lw'][self.whitening]['ms']
                else:
                    Lw = net.meta['Lw'][self.whitening]['ss']

        vecs = extract_vectors(net, image_list, self.image_size, transform, ms=ms, msp=msp)
        vecs = vecs.numpy()

        if Lw is not None:
            # whiten the vectors
            vecs = whitenapply(vecs, Lw['m'], Lw['P'])

        return vecs

    def get_matches(self, descriptor, query, images):
        # compute scores
        scores = np.dot(descriptor.T, query)

        adjacencie_list = {}
        threshold = 0.5
        for i in range(scores.shape[0]):
            indexes = np.where(scores[i, :] > threshold)
            if images[i] not in adjacencie_list:
                adjacencie_list[images[i]] = [images[j] for j in indexes[0] if images[i] != images[j]]

        return adjacencie_list


def config_imname(cfg, i):
    return os.path.join(cfg['dir_images'], cfg['imlist'][i] + cfg['ext'])


def config_qimname(cfg, i):
    return os.path.join(cfg['dir_images'], cfg['qimlist'][i] + cfg['qext'])


PRETRAINED_NETWORKS = {
    'retrievalSfM120k-resnet101-gem': 'http://cmp.felk.cvut.cz/cnnimageretrieval/data/networks/retrieval-SfM-120k/retrievalSfM120k-resnet101-gem-b80fb85.pth'}


def download_net(file_path, verbose):
    if os.path.isfile(file_path):
        if verbose:
            print(">>>> Network already downloaded")
    else:
        dst_dir = file_path.split("/")[:-1]
        dst_dir = "/".join(dst_dir)
        if verbose:
            print(dst_dir)
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir)
        os.system('wget {} -O {}'.format(PRETRAINED_NETWORKS['retrievalSfM120k-resnet101-gem'], file_path))
