from helper import DatasetLoader
from pipeline import Pipeline
from stepImageOverlap import rgbHistChi2Overlap, resNetCosineOverlap, resNetGemPoolOverlap
from util import storageStep, printStep


def runRGBHist(imagesFolderUniqueName):
    pipeline_params = {
        'image_loader': DatasetLoader(imagesFolderUniqueName)
    }

    steps = [
        rgbHistChi2Overlap.RGBHistChi2Overlap()
    ]

    gsfm = Pipeline(pipeline_params, steps)
    return gsfm.run()


def runResNet(imagesFolderUniqueName):
    pipeline_params = {
        'image_loader': DatasetLoader(imagesFolderUniqueName)
    }

    steps = [
        resNetCosineOverlap.ResNetCosineOverlap()
    ]

    gsfm = Pipeline(pipeline_params, steps)
    return gsfm.run()


def runResNetGemPooling(imagesFolderUniqueName):
    pipeline_params = {
        'image_loader': DatasetLoader(imagesFolderUniqueName)
    }

    steps = [
        resNetGemPoolOverlap.ResNetGemPoolOverlap()
    ]

    gsfm = Pipeline(pipeline_params, steps)
    return gsfm.run()


def runSaveDataOverlap(imagesFolderUniqueName):
    pipeline_params = {
        'image_loader': DatasetLoader(imagesFolderUniqueName)
    }

    steps = [
        resNetGemPoolOverlap.ResNetGemPoolOverlap(),
        printStep.PrintIntermediateData(),
        storageStep.StoreIntermediateData('data/checkpoints', 'overlapData'),
        storageStep.LoadIntermediateData('data/checkpoints', 'overlapData'),
        printStep.PrintIntermediateData(),
    ]

    gsfm = Pipeline(pipeline_params, steps)
    return gsfm.run()
