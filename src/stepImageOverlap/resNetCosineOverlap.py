# allows to import scripts from parent directory
# SRC_DIRECTORY_PATH = pathlib.Path(__file__).parent.parent
# sys.path.append(str(SRC_DIRECTORY_PATH.resolve()))  # allows to import src modules

from intermediateStep import IntermediateStep, InterStepDataMembers, ImagePair

import torch
from torch import nn
from torchvision import models, transforms
from torch.autograd import Variable
from PIL import Image


class ResNetDescriptor:
    def __init__(self):
        # Initialize the model
        self.model = models.resnet18(weights=models.ResNet18_Weights.IMAGENET1K_V1)
        # Use the model object to select the desired layer
        self.layer = self.model._modules.get('avgpool')
        self.model.eval()
        self.scaler = transforms.Resize((224, 224))
        self.normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                              std=[0.229, 0.224, 0.225])
        self.to_tensor = transforms.ToTensor()

    def describe(self, image):
        t_img = Variable(self.normalize(self.to_tensor(self.scaler(image))).unsqueeze(0))
        # 3. Create a vector of zeros that will hold our feature vector
        #    The 'avgpool' layer has an output size of 512
        my_embedding = torch.zeros([1, 512, 1, 1])

        # 4. Define a function that will copy the output of a layer
        def copy_data(m, i, o):
            my_embedding.copy_(o.data)

        # 5. Attach that function to our selected layer
        h = self.layer.register_forward_hook(copy_data)
        # 6. Run the model on our transformed image
        self.model(t_img)
        # 7. Detach our copy function from the layer
        h.remove()
        # 8. Return the feature vector
        return torch.squeeze(my_embedding)


class ResNetCosineOverlap(IntermediateStep):
    def __init__(self, ):  # , images_set_path="data/capitole"):
        super().__init__('ImageOverlap')
        self.desc = ResNetDescriptor()
        self.dist = nn.CosineSimilarity(dim=0, eps=1e-6)
        self.imgs = []

    # def loadIndex(self, index_path="data/index.cpickle"):
    #     self.index = pickle.loads(open(index_path, "rb").read())
    #
    # def saveIndex(self, index_path="data/index.cpickle"):
    #     f = open(index_path, "wb")
    #     f.write(pickle.dumps(self.index))
    #     f.close()

    def calcIndex(self, loader):
        self.imgs = loader.images
        for imageCamera in self.imgs:  # list_images(self.dataset_path):
            # extract our unique image ID (i.e. the filename)

            image = Image.open(imageCamera.path)
            features = self.desc.describe(image)
            image.close()
            imageCamera.globalFeatures = features
        print("[INFO] done...indexed {} images".format(len(self.imgs)))

    def search(self, query):
        # initialize our dictionary of results
        results = {}
        # loop over the index
        for i, img in enumerate(self.imgs):
            if query == img:
                continue

            d = self.dist(img.globalFeatures, query.globalFeatures)
            d = 1 - d

            results[i] = d

        results = sorted(results.items(), key=lambda x: x[1])
        return results

    def createPairs(self, threshold=0.4):
        imagePairs = []
        adjacencyList = []
        for i in range(len(self.imgs)):
            adjacencyList.append([])

        for i, query in enumerate(self.imgs):
            for (k, v) in self.search(query):
                if v > threshold:
                    continue
                if k in adjacencyList[i] or i in adjacencyList[k]:
                    continue
                adjacencyList[i].append(k)
                adjacencyList[k].append(i)
                imagePairs.append(ImagePair(query, self.imgs[k], v))
        return imagePairs, adjacencyList

    def run(self, pipeline_params: dict, run_data: dict) -> dict:
        self.calcIndex(pipeline_params['image_loader'])
        (imagePairs, adjacencyList) = self.createPairs()
        return {
            InterStepDataMembers.vertices: [i.clearFeatureCache() for i in self.imgs],
            InterStepDataMembers.edges: imagePairs,
            InterStepDataMembers.adjacencyList: adjacencyList
        }
