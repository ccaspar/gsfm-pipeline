import numpy as np
import pytheia as pt
from scipy.spatial.transform import Rotation as R

from intermediateStep import IntermediateStep, InterStepDataMembers

class PyTheiaRotationAveraging(IntermediateStep):

    def __init__(self, verbose=False):
        super().__init__('pyTheiaRotationAveraging', verbose=verbose)

    def run(self, pipeline_params: dict, run_data: dict):

        # init the options to the rotation estimator
        rotation_estimator_options = pt.sfm.RobustRotationEstimatorOptions()
        rotation_estimator = pt.sfm.RobustRotationEstimator(rotation_estimator_options)

        init_rotations = {}
        view_pairs = {}
        for imagePair in run_data[InterStepDataMembers.edges]:
            id1 = imagePair.image1.index
            id2 = imagePair.image2.index

            init_rotations[id1] = np.array([0., 0., 0.])
            init_rotations[id2] = np.array([0., 0., 0.])

            two_view_info = pt.sfm.TwoViewInfo()

            two_view_info.focal_length_1 = imagePair.image1.K[0][0]
            two_view_info.focal_length_2 = imagePair.image2.K[0][0]
            two_view_info.position_2 = imagePair.t
            two_view_info.rotation_2 = imagePair.R

            view_pairs[(id1, id2)] = two_view_info

        # the result from this function is a map from viewId to orientations
        result = rotation_estimator.EstimateRotations(view_pairs, init_rotations)

        for key, value in result.items():
            if self.verbose:
                print(R.from_rotvec(value).as_matrix())
            run_data[InterStepDataMembers.vertices][key].globalRotation = R.from_rotvec(value).as_matrix()
        return run_data
